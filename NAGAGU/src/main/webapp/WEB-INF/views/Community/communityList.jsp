<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
		<style>
			@font-face {
			   font-family: 'KOMACON';
			   src:
			      url('https://cdn.jsdelivr.net/gh/projectnoonnu/noonfonts_seven@1.2/KOMACON.woff')
			      format('woff');
			   font-weight: normal;
			   font-style: normal;
			}
			
			body {
			   font-family: '만화진흥원체', 'KOMACON', KOMACON !important;
			   font-size: 15px;
			}
			
			.search-tap {
			   border-bottom: 1px solid #EAEAEA;
			}
			
			.caption {
			   border-bottom: 1px solid #EAEAEA;
			}
			
			.main-content {
			   margin: 0 0 100px 0;
			}
			
			.main-content a, .main-content a:link, .main-content a:hover {
			   text-decoration: none;
			   color: black;
			}
			
			img {
			   width: 100%;
			   height: 100%;
			}
			
			.img {
			   height: 30vh;
			}
			
			.img-wrap {
			   padding-top: 10px;
			   padding-bottom: 15px;
			}
			
			.profile {
			   display: flex;
			   justify-content: space-between;
			}
			
			.caption {
			   display: flex;
			   justify-content: space-around;
			}
			
			.hover-image {
			   transition: .5s ease;
			   opacity: 0;
			   position: absolute;
			   top: 60%;
			   left: 50%;
			   transform: translate(-50%, -50%);
			   -ms-transform: translate(-50%, -50%);
			}
			
			.img:hover .img {
			   opacity: 0.3;
			}
			
			.img:hover #test {
			   opacity: 1;
			}
			.pagination {
			  display: inline-block;
			  margin-bottom:30px;
			}
			.pagination a {
			  color: black;
			  float: left;
			  padding: 8px 16px;
			  text-decoration: none;
			}
			.pagination a:hover {
			  background-color: #ef902e;
			  color:black;
			}		
			.search-tap a:hover{
				color: #ef900e !important;
				transform: scale(1.2);
			}
		</style>
	</head>
	
	<body>
	   <!-- SECTION: content -->
		<!-- content start -->
		<div class="container main-content">
			<div class="content">
		    	<div class="search-tap">
		        	<div class="caption py-5">
			            <a href=#>ALL</a> <a href=#>책상</a> <a href=#>의자</a> <a href=#>캐비넷</a>
			            <a href=#>침대</a> <a href=#>서랍장</a> <a href=#>협탁</a> <a href=#>화장대</a>
			            <a href=#>콘솔</a> <a href=#>기타</a>
		         	</div>
		         	<div class="row m-0 my-2">
						<table>
							<tr>
								<td>
									<select class="form-control mr-2">
										<option selected>최신순</option>
										<option>조회순</option>
										<option>인기순</option>
									</select>
								</td>
								<td>&nbsp;</td>
								<td>
									<select class="form-control">
										<option selected>전체보기</option>
										<option>사진</option>
										<option>후기</option>
									</select>
								</td>
							</tr>
						</table>
						<div class="col-lg-3 col-sm-4 ml-auto">
			        		<div class="input-group">
			           			<input type="text" class="form-control" placeholder="Search for..."> 
		           				<span class="input-group-btn ml-3">
		              				<button class="btn btn-outline-secondary btn-md my-2 my-sm-0" type="submit">Search</button>
		           				</span>
			        		</div>
			        	<!-- /input-group -->
		     			</div>
		     		<!-- /.col-lg-6 -->
				  	</div>
				<!-- div.row -->
				</div>
		<!-- images start -->
			<div class="row pt-2 mt-4">
		    	<div class="col-md-4 img-wrap">
		      		<div class="profile">
		        		<div>
		            		<a href="other_mypage.my"> <i class="far fa-user fa-2x"></i>
		               			<img src="" alt="" class="src">hadong
		            		</a>
		         		</div>
		         		<div>조회수1212</div>
		      		</div>
		      		<a href="${pageContext.request.contextPath}/community_detail.cm">
		      			<div class="img">
		            		<img src="https://images.unsplash.com/photo-1552584010-ca8bbbd5bd18?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=400&fit=max&ixid=eyJhcHBfaWQiOjYxOTE2fQ" class="img" />
			            	<div class="hover-image" id="test">
				               	<a href="#"> 
				               		<span class="button"> <i class="far fa-heart fa-2x" id="far"></i></span>75
				               	</a> 
				               	<a href="">
				               		<i class="far fa-share-square fa-2x"></i>
				               	</a>
			            	</div>
		         		</div>
		         	</a>
		      		<div class="img-tag">
		         		<span>#tag1</span> <span>#tag1</span> <span>#tag1</span> <span>#tag1</span><br />
		      		</div>
		   		</div>
		   		<div class="col-md-4 img-wrap">
		      		<div class="profile">
		        		<div>
		            		<i class="far fa-user fa-2x"></i> <img src="" alt="" class="src">hadong
		         		</div>
		         		<div>조회수1212</div>
		      		</div>
		      		<a href="">
		      			<div class="img">
		            		<img src="https://images.unsplash.com/photo-1552584010-ca8bbbd5bd18?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=400&fit=max&ixid=eyJhcHBfaWQiOjYxOTE2fQ" class="img" />
		            		<div class="hover-image" id="test">
			               		<a href="#"> 
			               			<span class="button"> <i class="far fa-heart fa-2x" id="far"></i></span>75
			               		</a> 
			               		<a href="">
			               			<i class="far fa-share-square fa-2x"></i>
			               		</a>
		            		</div>
		         		</div>
		         	</a>
		      		<div class="img-tag">
		         		<span>#tag1</span> <span>#tag1</span> <span>#tag1</span> <span>#tag1</span><br />
		      		</div>
		   		</div>
				<div class="col-md-4 img-wrap">
					<div class="profile">
						<div>
							<i class="far fa-user fa-2x"></i> <img src="" alt="" class="src">hadong
						</div>
						<div>조회수1212</div>
					</div>
					<a href=""><div class="img">
							<img src="https://images.unsplash.com/photo-1552584010-ca8bbbd5bd18?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=400&fit=max&ixid=eyJhcHBfaWQiOjYxOTE2fQ" class="img" />
							<div class="hover-image" id="test">
								<a href="#"> 
									<span class="button"> <i class="far fa-heart fa-2x" id="far"></i></span>75
								</a> 
								<a href="">
									<i class="far fa-share-square fa-2x"></i>
								</a>
							</div>
						</div></a>
					<div class="img-tag">
						<span>#tag1</span> <span>#tag1</span> <span>#tag1</span> <span>#tag1</span><br />
					</div>
				</div>
				<div class="col-md-4 img-wrap">
		      <div class="profile">
		         <div>
		            <i class="far fa-user fa-2x"></i> <img src="" alt="" class="src">hadong
		         </div>
		         <div>조회수1212</div>
		      </div>
		      <a href=""><div class="img">
		            <img
		               src="https://images.unsplash.com/photo-1552584010-ca8bbbd5bd18?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=400&fit=max&ixid=eyJhcHBfaWQiOjYxOTE2fQ"
		               class="img" />
		            <div class="hover-image" id="test">
		               <a href="#"> <span class="button"> <i
		                     class="far fa-heart fa-2x" id="far"></i>
		               </span>75
		               </a> <a href=""><i class="far fa-share-square fa-2x"></i></a>
		            </div>
		         </div></a>
		      <div class="img-tag">
		         <span>#tag1</span> <span>#tag1</span> <span>#tag1</span> <span>#tag1</span><br />
		      </div>
		   </div>
		   <div class="col-md-4 img-wrap">
		      <div class="profile">
		         <div>
		            <i class="far fa-user fa-2x"></i> <img src="" alt="" class="src">hadong
		         </div>
		         <div>조회수1212</div>
		      </div>
		      <a href=""><div class="img">
		            <img
		               src="https://images.unsplash.com/photo-1552584010-ca8bbbd5bd18?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=400&fit=max&ixid=eyJhcHBfaWQiOjYxOTE2fQ"
		               class="img" />
		            <div class="hover-image" id="test">
		               <a href="#"> <span class="button"> <i
		                     class="far fa-heart fa-2x" id="far"></i>
		               </span>75
		               </a> <a href=""><i class="far fa-share-square fa-2x"></i></a>
		            </div>
		         </div></a>
		      <div class="img-tag">
		         <span>#tag1</span> <span>#tag1</span> <span>#tag1</span> <span>#tag1</span><br />
		      </div>
		   </div>
		   <div class="col-md-4 img-wrap">
		      <div class="profile">
		         <div>
		            <i class="far fa-user fa-2x"></i> <img src="" alt="" class="src">hadong
		         </div>
		         <div>조회수1212</div>
		      </div>
		      <a href=""><div class="img">
		            <img
		               src="https://images.unsplash.com/photo-1552584010-ca8bbbd5bd18?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=400&fit=max&ixid=eyJhcHBfaWQiOjYxOTE2fQ"
		               class="img" />
		            <div class="hover-image" id="test">
		               <a href="#"> <span class="button"> <i
		                     class="far fa-heart fa-2x" id="far"></i>
		               </span>75
		               </a> <a href=""><i class="far fa-share-square fa-2x"></i></a>
		            </div>
		         </div></a>
		      <div class="img-tag">
		         <span>#tag1</span> <span>#tag1</span> <span>#tag1</span> <span>#tag1</span><br />
		      </div>
		   </div>
		   <div class="col-md-4 img-wrap">
		      <div class="profile">
		         <div>
		            <i class="far fa-user fa-2x"></i> <img src="" alt="" class="src">hadong
		         </div>
		         <div>조회수1212</div>
		      </div>
		      <a href=""><div class="img">
		            <img
		               src="https://images.unsplash.com/photo-1552584010-ca8bbbd5bd18?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=400&fit=max&ixid=eyJhcHBfaWQiOjYxOTE2fQ"
		               class="img" />
		            <div class="hover-image" id="test">
		               <a href="#"> <span class="button"> <i
		                     class="far fa-heart fa-2x" id="far"></i>
		               </span>75
		               </a> <a href=""><i class="far fa-share-square fa-2x"></i></a>
		            </div>
		         </div></a>
		      <div class="img-tag">
		         <span>#tag1</span> <span>#tag1</span> <span>#tag1</span> <span>#tag1</span><br />
		      </div>
		   </div>
		   <div class="col-md-4 img-wrap">
		      <div class="profile">
		         <div>
		            <i class="far fa-user fa-2x"></i> <img src="" alt="" class="src">hadong
		         </div>
		         <div>조회수1212</div>
		      </div>
		      <a href=""><div class="img">
		            <img
		               src="https://images.unsplash.com/photo-1552584010-ca8bbbd5bd18?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=400&fit=max&ixid=eyJhcHBfaWQiOjYxOTE2fQ"
		               class="img" />
		            <div class="hover-image" id="test">
		               <a href="#"> <span class="button"> <i
		                     class="far fa-heart fa-2x" id="far"></i>
		               </span>75
		               </a> <a href=""><i class="far fa-share-square fa-2x"></i></a>
		            </div>
		         </div></a>
		      <div class="img-tag">
		         <span>#tag1</span> <span>#tag1</span> <span>#tag1</span> <span>#tag1</span><br />
		      </div>
		   </div>
		   <div class="col-md-4 img-wrap">
		      <div class="profile">
		         <div>
		            <i class="far fa-user fa-2x"></i> <img src="" alt="" class="src">hadong
		         </div>
		         <div>조회수1212</div>
		      </div>
		      <a href=""><div class="img">
		            <img
		               src="https://images.unsplash.com/photo-1552584010-ca8bbbd5bd18?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=400&fit=max&ixid=eyJhcHBfaWQiOjYxOTE2fQ"
		               class="img" />
		            <div class="hover-image" id="test">
		               <a href="#"> <span class="button"> <i
		                     class="fa fa-heart fa-2x" id="far"></i>
		               </span>75
		               </a> <a href=""><i class="far fa-share-square fa-2x"></i></a>
		            </div>
		         </div></a>
		      <div class="img-tag">
		         <span>#tag1</span> <span>#tag1</span> <span>#tag1</span> <span>#tag1</span><br />
		      </div>
		   </div>
		
		</div>
		<!-- images end -->
		</div>
		<!-- SECTION: content  -->
		
		</div>
		<!-- pagenation -->
		<div class="row justify-content-center">
			<div class="pagination">
			  <a href="#">&laquo;</a>
			  <a href="#">1</a>
			  <a href="#">2</a>
			  <a href="#">3</a>
			  <a href="#">4</a>
			  <a href="#">5</a>
			  <a href="#">6</a> 
			  <a href="#">&raquo;</a>
			</div>
		</div>
		<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
		<script src="https://kit.fontawesome.com/97dbc99ea1.js" crossorigin="anonymous"></script>
		<!-- jquery -->
		<script>
		$(function(){
			  $(document).on("click","#far",function addClass() {
			    if($(this).attr('data-prefix') === "far"){
			      $(this).removeClass("far fa-heart fa-2x");
				   $(this).addClass("fas fa-heart fa-2x");
			    }else{
			      $(this).removeClass("fas fa-heart fa-2x");
			      $(this).addClass("far fa-heart fa-2x");
			    }
			  });
			});
		</script>
	</body>
</html>
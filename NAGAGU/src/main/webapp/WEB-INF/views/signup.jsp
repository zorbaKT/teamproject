<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>회원가입 폼</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="./resources/css/signup.css">
    <style>
    	@font-face {
				font-family: 'KOMACON';
				src:
					url('https://cdn.jsdelivr.net/gh/projectnoonnu/noonfonts_seven@1.2/KOMACON.woff')
					format('woff');
				font-weight: normal;
				font-style: normal;
		}
			
		.order-body {
			font-family: '만화진흥원체', 'KOMACON', KOMACON;
			font-size: 15px;
		}
    </style>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
</head>

<body class="order-body">
    <div class="cbody">
        <div class="signup_form">
            <h1 class="signup_title">회원가입</h1>
            <div class="singup_sns">
                <p class="signup_sns_title">SNS계정으로 가입</p>
                <ol class="signup_sns_list">
                    <li class="signup_sns_list_item">
                        <img src="https://testkj.s3.ap-northeast-2.amazonaws.com/images/login_kakao.png">
                    </li>
                    <li class="signup_sns_list_item">
                        <img src="https://testkj.s3.ap-northeast-2.amazonaws.com/images/login_btn_naver.PNG">
                    </li>
                </ol>
            </div>
            <form class="singup_form" anction="./loginform.html">
                <div class="signup_form_email">
                    <div class="signup_form_label">이메일</div>
                    <div class="signup_form_input">
                        <div class="input_email form_group">
                            <div class="input_email_local">
                                <input class="form_input" id="email1" name="email1" placeholder="이메일" size="1">
                            </div>
                            <div class="input_email_seperator">@</div>
                            <div class="input_email_domain">
                                <select id="email2" name="email2" class="form_input">
                                    <option disabled>선택해주세요</option>
                                    <option value="naver.com">naver.com</option>
                                    <option value="hanmail.net">hanmail.net</option>
                                    <option value="gmail.com">gmail.com</option>
                                    <option value="hotmail.com">hotmail.com</option>
                                    <option value="nate.com">nate.com</option>
                                    <option value="_manual">직접입력</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="error_next_box" id="emailMsg" style="display: hidden;" aria-live="assertive"></div>
                </div>
                <div class="signup_form_pass1 form_group">
                    <div class="signup_form_label">비밀번호</div>
                    <div class="signup_form_input">
                        <input type="password" id="pass1" name="member_pass" class="form_input" placeholder="8자 이상 영문, 숫자, 특수문자를 사용하세요.">
                    </div>
                    <div class="error_next_box" id="pass1Msg" style="display: hidden;" aria-live="assertive"></div>
                </div>
                <div class="signup_form_pass2 form_group">
                    <div class="signup_form_label">비밀번호 확인</div>
                    <div class="signup_form_input">
                        <input type="password" id="pass2" name="member_pass_check" class="form_input">
                    </div>
                    <div class="error_next_box" id="pass2Msg" style="display: hidden;" aria-live="assertive"></div>
                </div>
                <div class="signup_form_nickname form_group">
                    <div class="signup_form_label">별명</div>
                    <div class="signup_form_input">
                        <input type="text" id="nickname" name="member_nickname" class="form_input">
                    </div>
                    <div class="error_next_box" id="nickMsg" style="display: hidden;" aria-live="assertive"></div>
                </div> 
                <div class="signup_form_term form_group">   
                    <div class="signup_form_label">약관 동의</div>
                    <div class="signup_form_term_body">
                        <div class="form_term">
                            <div class="term_all">
                                <div class="form_check checkbox_input">
                                    <label class="form_check_label">
                                        <input type="checkbox" class="form_check" id="check_all" name="check_all">
                                        <span class="check_img"></span>
                                        <span class="term_all_text">전체동의</span>
                                    </label>
                                </div>
                            </div>
                            <div class="term_list">
                                <div class="term_agree_row">
                                    <div class="form_check checkbox_input">
                                        <label class="form_check_label">
                                            <input type="checkbox" class="form_check" id="check_service" name="check_service">
                                            <span class="check_img"></span>
                                            <span class="term_all_text">이용약관</span>
                                        </label>
                                    </div>
                                </div>
                                <div class="term_agree_row">
                                    <div class="form_check checkbox_input">
                                        <label class="form_check_label">
                                            <input type="checkbox" class="form_check" id="check_privacy" name="check_privacy">
                                            <span class="check_img"></span>
                                            <span class="term_all_text">개인정보취급</span>
                                        </label>
                                    </div>
                                </div>
                                <div class="term_agree_row">
                                    <div class="form_check checkbox_input">
                                        <label class="form_check_label">
                                            <input type="checkbox" class="form_check" id="check_mailing" name="check_mailing">
                                            <span class="check_img"></span>
                                            <span class="term_all_text">이벤트, 프로모션 알림 메일 및 SMS 수신 (선택)</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="error_next_box" id="termMsg" style="display: hidden;" aria-live="assertive"></div>
                <button id="btn_submit" class="signup_form_submit" type="submit">회원가입</button>
            </form>
        </div>
    </div>



   
    <script>
        var emailFlag = false;
        var passFlag = false;
        var nickFlag = false;
        var submitFlag = false;
        var termFlag = false;

        function showErrorMsg(obj, msg) {
        obj.attr("class", "error_next_box");
        obj.html(msg);
        obj.show();
        }

        var isShift = false;

        function checkShiftUp(e) {
            if (e.which && e.which == 16) {
                isShift = false;
            }
        }

        function checkShiftDown(e) {
            if (e.which && e.which == 16) {
                  isShift = true;
            }
         }

        function checkCapslock(e) {
            var myKeyCode = 0;
            var myShiftKey = false;

            if (window.event) { // IE
                myKeyCode = e.keyCode;
                myShiftKey = e.shiftKey;
            } else if (e.which) { // netscape ff opera
                myKeyCode = e.which;
                myShiftKey = isShift;
            }
            var oMsg = $("#pswd1Msg");

            if ((myKeyCode >= 65 && myKeyCode <= 90) && !myShiftKey) {
                showErrorMsg(oMsg,"Caps Lock이 켜져 있습니다.");
            } else if ((myKeyCode >= 97 && myKeyCode <= 122) && myShiftKey) {
                showErrorMsg(oMsg,"Caps Lock이 켜져 있습니다.");
            } else {
                oMsg.hide();
            }
        }

        function setFocusToInputObject(obj) {
            if(submitFlag) {
                submitFlag = false;
                obj.focus();
            }
        }

        $("#email1").blur(function(){
            emailFlag=false;
            checkEmail1("first");
        });

        $("#email2").blur(function(){
            emailFlag=false;
            checkEmail2("first");
        });
        
        $("#pass1").blur(function(){
            passFlag=false;
            checkPass1();
        }).keyup(function(event){
            checkShiftUp(event);
        }).keypress(function(event){
            checkCapslock(event);
        }).keydown(function(event){
            checkShiftDown(event);
        });

        $("#pass2").blur(function(){
            passFlag=false;
            checkPass2();
        }).keyup(function(event){
            checkShiftUp(event);
        }).keypress(function(event){
            checkCapslock(event);
        }).keydown(function(event){
            checkShiftDown(event);
        }); 

        $("#nickname").blur(function(){
            nickFlag=false;
            checkNick();
        });

        function checkEmail1(event) {
            var email1 = $("#email1").val();
            var oMsg = $("#emailMsg");
            var input1 = $("#email1");

            if (email1=="") {
                showErrorMsg(oMsg, "필수 정보입니다.");
                return false;
            } else {
            oMsg.hide();
            return true;
            }

            emailFlag = false;

            return false;
        }

        function checkEmail2(event) {
            var email2 = $("#email2").val();
            var oMsg = $("#emailMsg");
            var input2 = $("#email2");
            
            if (email2=="") {
                showErrorMsg(oMsg, "필수 정보입니다.");
                return false;
            } else {
            oMsg.hide();
            if (email1!="") {
                emailFlag=true;
                return true;
            }
            return true;
            }

            emailFlag = false;

            return false;
        }

        function checkPass1(event) {
            var email = $("#email1").val()+"@"+$("#email2").val();
            var pass1 = $("#pass1").val();
            var oMsg = $("#pass1Msg");
            var oInput = $("#pass1");
            
            if (pass1=="") {
                showErrorMsg(oMsg, "필수 정보입니다.");
                setFocusToInputObject(oInput);
                return false;
            } else {
            oMsg.hide();
            return true;
            }

            passFlag = false;

            return true;
        }

        function checkPass2(event) {
            var email = $("#email1").val()+"@"+$("#email2").val();
            var pass1 = $("#pass1").val();
            var pass2 = $("#pass2").val();
            var oMsg = $("#pass2Msg");
            var oInput = $("#pass2");
            
            if (pass2=="") {
                showErrorMsg(oMsg, "필수 정보입니다.");
                setFocusToInputObject(oInput);
                return false;
            } else {
            oMsg.hide();
            }
            
            if (pass1 != pass2) {
                showErrorMsg(oMsg,"비밀번호가 일치하지 않습니다.");
                setFocusToInputObject(oInput);
                return false;
            } else {
                oMsg.hide();
                passFlag = true;
                return true;
            }
                passFlag = false;

            return true;
        }

        function checkNick(event) {
            var nick = $("#nickname").val();
            var oMsg = $("#nickMsg");
            var oInput = $("#nickname");
            var checkLength = /^[가-힣A-Za-z0-9_]{2,12}$/;
            
            if (nick=="") {
                showErrorMsg(oMsg, "필수 정보입니다.");
                return false;
            } else {
                oMsg.hide();
                nickFlag=true;
            }

            if (!checkLength.test(nick)) {
                showErrorMsg(oMsg, "별명은 최소 2자, 최대 12자 까지 입력가능 합니다.[특수문자 불가]");
                setFocusToInputObject(oInput);
                return false;
            } else {
            oMsg.hide();
            return true;
            }

            nickFlag = false;

            return false;
        }

        function checkterm() {
            var res = true;
            var oMsg = $("#termMsg");

        if ($("#check_service").is(":checked") == false || $("#check_privacy").is(":checked") == false) {
            showErrorMsg(oMsg, "이용약관과 개인정보 수집 및 이용에 대한 안내 모두 동의해주세요.");
            res = false;
        } else {
            oMsg.hide();
        }

        return res;
        }

        $("#btn_submit").click(function(event) {
            if(emailFlag && passFlag && nickFlag) {
                if (checkterm()==true) {
                    alert("완료");
                    mainSubmit();
                }
                else {
                    setFocusToInputObject("#check_service");
                    return false;
                }
            }
            else {
                return false;
            }
        });

        function mainSubmit() {

            if(emailFlag && passFlag && nickFlag) {
                $("#join_form").submit();
            } else {
                submitOpen();
                return false;
            }
        }

        function submitOpen() {
            $("#btn_submit").attr("disabled",false);
        }

        /*-- 약관 확인 --*/
        //약관 전체 승인
        function setTerms() {
            if ($("#check_all").is(":checked")) {
                $("#check_service").prop("checked",true);
                $("#check_privacy").prop("checked",true);
                $("#check_mailing").prop("checked",true);
            } else {
                $("#check_service").prop("checked",false);
                $("#check_privacy").prop("checked",false);
                $("#check_mailing").prop("checked",false);
            }
        }
        function viewterm_() {

        if( !$("#check_service").is(":checked") || !$("#check_privacy").is(":checked") || !$("#check_mailing").is(":checked")) {
            $("#check_all").prop("checked",false);
        }

        if( $("#check_service").is(":checked") && $("#check_privacy").is(":checked") && $("#check_mailing").is(":checked")) {
            $("#check_all").prop("checked",true);
        }

        return true;
        }
        

    $(document).ready(function(){
        $("#check_all").prop("checked",false);
        setTerms();

        $("#check_all").click(function() {
            setTerms();
        })

        $("#check_service").click(function() {
            viewterm_();
        })

        $("#check_privacy").click(function() {
            viewterm_();
        })

        $("#check_mailing").click(function() {
             viewterm_();
        })

    });
    </script>
    
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>

</html>
package com.spring.academy;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
@Controller
public class AcademyController {
	
	@Autowired
	private AcademyServiceImpl AcademyService;
	
	@RequestMapping(value = "/classlist.ac")
	public ModelAndView ClassList(ModelAndView result)  {
		// view 화면인 main.jsp에 DB로 부터 읽어온 데이터를 보여준다.
		
		// addObject view에 넘어가는 데이터
		List<ClassVO> classList = AcademyService.getCls();
		int listcount = AcademyService.getCount();
		
		result.addObject("classList", classList);
		result.addObject("listcount", listcount);
		result.setViewName("Academy/classList");
		
		return result;
	}
	
	@RequestMapping(value = "/classdetail.ac")
	public String ClassDetail() {
		
		return "Academy/detail";
	}
	
	@RequestMapping(value = "/classreservation.ac")
	public String ClassReservation() {
		
		return "Academy/reservation";
	}
	
	@RequestMapping(value = "/classform.ac")
	public String ClassForm() {
		
		return "Academy/classForm";
	}
}

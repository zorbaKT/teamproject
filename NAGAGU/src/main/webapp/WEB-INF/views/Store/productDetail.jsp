<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ page import="com.spring.store.ProductVO" %>
<%@ page import = "java.text.SimpleDateFormat" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<%
	System.out.println("ProductDetail.jsp----------------------------------");

	ProductVO vo = (ProductVO)request.getAttribute("productVO");
	String PRODUCT_CATEGORY = (String)request.getAttribute("PRODUCT_CATEGORY");
	int PRODUCT_NUM = ((Integer)request.getAttribute("PRODUCT_NUM")).intValue();
	int nowpage = ((Integer)request.getAttribute("page")).intValue();
	System.out.println("product_color="+vo.getPRODUCT_COLOR());
	String PRODUCT_COLOR = vo.getPRODUCT_COLOR();
	String PRODUCT_SIZE = vo.getPRODUCT_SIZE();
	String PRODUCT_OPTION = vo.getPRODUCT_OPTION();
	System.out.println("pic_count="+vo.getPRODUCT_PIC_COUNT());

	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");

%>  
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
		
		<style>
@charset "UTF-8";

img {
	max-width: 100%;
	height: auto;
}

table {
	font-size: 0.98em;
}

.nav_tab {
	background-color: #FEE100;
}

.navbar-nav>li {
	padding-left: 30px;
	padding-right: 30px;
}

/*안 먹힘*/
@media screen and (max-width: 375px) { 
	.nav-item {
		font-size:0.5em !important;
	}
	
}

.name {
	font-weight: bold;
}


.smallfont {
	font-size: 0.7em;
}

a {
	color: gray;
	text-decoration: none;
}

.reviews_table {
	color: #212529;
}

.row_ship_info {
	padding:3px;
}		
		
		
		
			@font-face {
				font-family: 'KOMACON';
				src:
					url('https://cdn.jsdelivr.net/gh/projectnoonnu/noonfonts_seven@1.2/KOMACON.woff')
					format('woff');
				font-weight: normal;
				font-style: normal;
			}
			
			.Precautions dl dd {
				font-size: 0.7rem;
			}
			
			#subject {
				font-size: 1.0rem;
			}
			
			.class-detail-container {
				margin-top: 100px;
				margin-bottom: 100px;
			}
			
			.hr-class {
				width: 100%;
				color: #f6f6f6;
				margin-top: 100px;
				margin-bottom: 100px;
			}
			
			.order-body {
				font-family: '만화진흥원체', 'KOMACON', KOMACON;
			}
			
			.sticky {
				padding-top: 5%;
				z-index:2;
				position: -webkit-sticky;
				position: sticky;
				background-color: #FFFFFF;
				top: 0;
			}
			
			.sticky2 {
				z-index:2;
				position: -webkit-sticky;
				position: sticky;
				background-color: #FFFFFF;
				top: 20px;
			}
			
			.nav-item .nav-link {
				color: #9d9d9d;
			}
			
			.comments_table {
				font-size: 1rem;
			}
			
			@media ( max-width : 700px) {
				.comments_table {
					font-size: 0.7rem;
				}
			}
			
			div.col-2 img {
				width: 100%;
				height: 100%;
			}
			
			
			.rep_content {
				font-size: 1.0em;
			}
			
			hr {
				background-color: #EF902E;
			}
		
		
			.review_img {
				width:100px;
				height:100px;
			}
			.review_add_section {
				background-color: #FAFAFA;
			}

			.review_hidden {
				display:none;
			}
			.review_file_preview {
				width:100px;
				height:100px;
			}
			.review_sum {
				width: 100%; 
				margin: 0 auto;
			}
			.review_grade {
				width: 80% !important; 
				margin: 0 auto !important;			
			}
			.review_file_upload {
				width: 80% !important; 
				margin: 0 auto !important;					
			}
			.qna_sum {
				width: 100%; 
				margin: 0 auto;
			}
			
		</style>
		<script>
		
		</script>
	</head>
	<body class="order-body">
		<!-- content start -->
		<div class="container class-detail-container">
			<div class="col-12 text-center" style="padding-bottom: 5%;">
				<div id="carouselExampleFade" class="carousel slide carousel-fade" data-ride="carousel">
 					<div class="carousel-inner text-center">
    					<div class="carousel-item active">
    						<img src=<%=vo.getPRODUCT_PIC().split(",")[0]%> alt="" class="img-responsive img-rounded w-100" width="100%" />
    					</div>
    					<div class="carousel-item">
    						<img src=<%=vo.getPRODUCT_PIC().split(",")[1]%>	alt="" class="img-responsive img-rounded w-100" width="100%" />
    					</div>
    					<div class="carousel-item">
      						<img src=<%=vo.getPRODUCT_PIC().split(",")[2]%>	alt="" class="img-responsive img-rounded w-100" />
    					</div>
    					<div class="carousel-item">
      						<img src=<%=vo.getPRODUCT_PIC().split(",")[3]%>	alt="" class="img-responsive img-rounded w-100" />
    					</div>
 					</div>
  					<a class="carousel-control-prev" href="#carouselExampleFade" role="button" data-slide="prev">
    					<span class="carousel-control-prev-icon" aria-hidden="true"></span>
    					<span class="sr-only">Previous</span>
  					</a>
  					<a class="carousel-control-next" href="#carouselExampleFade" role="button" data-slide="next">
    					<span class="carousel-control-next-icon" aria-hidden="true"></span>
    					<span class="sr-only">Next</span>
  					</a>
				</div>
			</div>
			<div class="row">
				<div class="col-8">
					<div style="line-height: 0.5em;" id="t1">
						<dl>
							<dt><h3>메이킹퍼니처 | 원목테이블</h3><br></dt>
							<dl><h5>합판과 나사못을 전혀 사용하지 않는 가구</h5></dl>
						</dl>
					</div>
					<div class="row sticky">
						<div class="col-12">
							<ul class="nav nav-tabs nav-fill h-30">
								<li class="nav-item">
									<a class="nav-link" href="#t1"><h5>상품정보</h5></a>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="#t2"><h5>리뷰</h5></a>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="#t3"><h5>Q&A</h5></a>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="#t4"><h5>배송/환불</h5></a>
								</li>
							</ul>
						</div>
					</div>
					
					<div class="row justify-content-center">
					<%
						if (vo.getPRODUCT_PIC_COUNT() > 0) {
							if (vo.getPRODUCT_PIC_COUNT() > 3) { //본문 이미지는 i=4부터 출력
								System.out.println("vo.getPRODUCT_PIC_COUNT()=" + vo.getPRODUCT_PIC_COUNT());
								for (int i = 4; i < vo.getPRODUCT_PIC_COUNT(); i++) {
					%>
									<img src=<%=vo.getPRODUCT_PIC().split(",")[i]%> alt="" class="img-responsive img-rounded" />
					<%
								}
							} else { //본문 사진이 없을 경우
					%>
								<table width="50%" border="0" cellpadding="0" cellspacing="0" align="center" valign="middle">
									<tr align="center" valign="middle">
										<td align="right"><font size=2>상세 정보 입력 전입니다.</font></td>
									</tr>
								</table>
					<%
							}
					%>
					<br /><br />
					<%
						} else {
					%>
							<!-- vo.getPRODUCT_PIC_COUNT() = 0 인 경우-->
							<table width="50%" border="0" cellpadding="0" cellspacing="0" align="center" valign="middle">
								<tr align="center" valign="middle">
									<td align="right"><font size=2>등록된 상품 내용이 없습니다.</font></td>
								</tr>
							</table>
					<%
						}
					%>
				</div>
				<!-- 상세 사진정보 끝 -->
				
				
				<span id="t2"></span>
				<br /><br /><hr />
				
				<!-- 리뷰 테이블 시작 -->
				<h3 >Review</h3>
				<br /><br />
				<div class="reviews_table" >
				
				





					<div class="review_sum justify-content-center" >
						<div class="row justify-content-center">
							<div class="col-1 justify-content-end">
								<img src="${pageContext.request.contextPath}/resources/images/Community/peko.png" alt="" class="img-circle">
							</div>
		
							<div class="col-11">
								<div class="row">
									<div class="col-10 justify-content-end name">김이름</div>
									<div class="col-2 justify-content-center smallfont">2020-01-08</div>
								</div>
								<div class="row">
									<div class="col-2 justify-content:space-end">★★★☆☆</div>
									<div class="col-8"></div>
								</div>
								<div class="row "> <!-- 사진 -->
									<img
										src="${pageContext.request.contextPath}/resources/images/Store/tables/table01/table01_01.jpg"
										class="review_img">
									&nbsp;&nbsp;							
									<img
										src="${pageContext.request.contextPath}/resources/images/Store/tables/table01/table01_02.jpg"
										class="review_img">
									&nbsp;&nbsp;							
									<img
										src="${pageContext.request.contextPath}/resources/images/Store/tables/table01/table01_03.jpg"
										class="review_img">
								</div>								
							
								<div class="row rep_content">후기입니다~ 식탁이라 열심히 행주로 닦아주며 쓰고 있고 아이들이 그림
									그리다가 색연필 자국이 나면 그냥 지우개로 지우며 쓰고 있어요. 만족합니다.</div>
							</div>
						</div>
					</div>

				
				
				
				
				
				
				
				
				
				
				
					<div class="review_sum justify-content-center" >
						<div class="row justify-content-center">
							<div class="col-1 justify-content-end">
								<img src="${pageContext.request.contextPath}/resources/images/Community/peko.png" alt="" class="img-circle">
							</div>
		
							<div class="col-11">
								<div class="row">
									<div class="col-10 justify-content-end name">김이름</div>
									<div class="col-2 justify-content-center smallfont">2020-01-08</div>
								</div>
								<div class="row"><!-- 후기사진 -->
									<div class="col">★★★☆☆</div>
								</div>
								<div class="row rep_content">식탁이라 열심히 행주로 닦아주며 쓰고 있고 아이들이 그림
									그리다가 색연필 자국이 나면 그냥 지우개로 지우며 쓰고 있어요. 만족합니다.</div>
							</div>
						</div>
					</div>
					<div class="review_sum justify-content-center">
						<div class="row">
							<div class="col-1 justify-content-end">
								<img src="${pageContext.request.contextPath}/resources/images/Store/table.jpg" alt="" class="img-circle">
							</div>
		
							<div class="col-11 ">
								<div class="row">
									<div class="col-10 justify-content-end name">김이름</div>
									<div class="col-2 justify-content-center smallfont">2020-01-08</div>
								</div>
								<div class="row ">
									<div class="col-2 justify-content:space-end">★★★☆☆</div>
									<div class="col-8"></div>
								</div>
								<div class="row rep_content">주문후기 주문후기 주문후기</div>
							</div>
						</div>
					</div>
					<div class="review_sum justify-content-center" >
						<div class="row">
							<div class="col-1 justify-content-end">
								<img src="${pageContext.request.contextPath}/resources/images/Community/peko.png" alt="" class="img-circle">
							</div>
		
							<div class="col-11 ">
								<div class="row">
									<div class="col-10 justify-content-end name">김이름</div>
									<div class="col-2 justify-content-center smallfont">2020-01-08</div>
								</div>
								<div class="row ">
									<div class="col-2 justify-content:space-end">★★★☆☆</div>
									<div class="col-8"></div>
								</div>
								<div class="row rep_content">주문후기 주문후기 주문후기</div>
							</div>
						</div>
					</div>
					<br />
					<br />
					<div class="review_add_section justify-content-center"	style="width: 100%; margin: 0 auto;">
						<div class="row justify-content-center pt-3 pb-3">
							<button class="btn btn-dark btn-md review_add">댓글 달기</button>
						</div>
						<form id="reviewform" name="reviewform" method="post" enctype="multipart/form-data">
							<div class="review_hidden">
								<div class="row justify-content-center ">
									<textarea name="REVIEW_CONTENT" placeholder="후기를 작성해주세요!" cols="90%" rows="5"></textarea>		
								</div>
								<div class="row review_grade">
									평점 : (1.0~5.0) &nbsp;<input type="text" name="REVIEW_GRADE">
								</div>
								<div class="review_file_upload"  >
									<div class="row pt-2 pb-2" >
										<div class="input_wrap">
											<input type="file" name="REVIEW_FILE" id="input_imgs" multiple>
										</div>	
									</div>
									<div>
										<div class="imgs_wrap">
											<img id="img" >
										</div>
									</div>
								</div>	
								<div class=" row justify-content-center pb-3">
									<button class="btn btn-dark btn-sm" id="insert_comment" style="cursor: pointer;">등록</button>
								</div>	
							</div>	
						</form>	
					</div>
					<br />
				</div>
				<!-- 리뷰 테이블 끝 -->
		
				<span id="t3"></span>
				<br />
				<br />
				<hr />
				
				<!-- Q&A 테이블 시작 -->
				<h3 >Q&A</h3>
				<br />
				<br />
				<div class="qna_table" style="color: #212529;">
					<div class="qna_sum"></div>
					<div class="qna_sum justify-content-center"	>
						<div class="row">
							<div class="col-1 justify-content-end">
								<img src="${pageContext.request.contextPath}/resources/images/Community/peko.png" alt="" class="img-circle">
							</div>
							<div class="col-11">
								<div class="row">
									<div class="col-10 justify-content-end name">박이름</div>
									<div class="col-2 justify-content-center smallfont">2020-01-08</div>
								</div>
								<div class="row ">
									<div class="col rep_content">문의합니다 문의합니다 문의합니다</div>
								</div>
								<div class="row" style="height: 20px;">
									<a href="#" class="smallfont">답글달기</a> &nbsp;&nbsp; 
									<a href="#"	class="smallfont">신고하기</a>
								</div>
							</div>
						</div>
					</div>
					<br />
					<div class="qna_sum justify-content-center" >
						<div class="row">
							<div class="col-1 justify-content-end">
								<img src="${pageContext.request.contextPath}/resources/images/Community/peko.png" alt="" class="img-circle">
							</div>
							<div class="col-11">
								<div class="row">
									<div class="col-10 justify-content-end name">박이름</div>
									<div class="col-2 justify-content-center smallfont">2020-01-08</div>
								</div>
								<div class="row ">
									<div class="col justify-content:space-end rep_content">
										안녕하세요	주말 1명 참여 가능한 가장 빠른 클래스가 언제인가요?</div>
								</div>
								<div class="row" style="height: 20px;">
									<a href="#" class="smallfont">답글달기</a> &nbsp;&nbsp; 
									<a href="#"	class="smallfont">신고하기</a>
								</div>
							</div>
						</div>
					</div>
		
					<br />
					<div class="qna_reply_wrap"	style="width: 88%; margin: 0 auto; background-color: #FAFAFA;">
						<div class="qna_reply " style="width: 95%; margin: 0 auto;">
							<div class="row justify-content-start ml-0  name">
								<i class="fab fa-replyd"></i>&nbsp;&nbsp;메이킹퍼니처
							</div>
							<div class="row justify-content-start ml-0 rep_content">안녕하세요 요번 주 토요일
								12시요~</div>
							<div class="row" style="height: 20px;">
								<a href="#" class="smallfont ml-0">&nbsp;&nbsp;&nbsp;&nbsp;답글달기</a>
								&nbsp;&nbsp; <a href="#" class="smallfont ml-0">신고하기</a>
							</div>
						</div>
					</div>
					<br />
					<br />
					<div class="qna_add justify-content-center" style="width: 100%; margin: 0 auto;">
						<div class="row justify-content-center">
							<div class="col-10">
								<textarea name="PICS_RE_CONTENT" class="col-12" rows="2"></textarea>
							</div>
							<div class="col-1">
								<div class=" row justify-content-center">
									<a class="btn btn-outline-dark btn-sm " onClick="location.href='#'" style="cursor: pointer;">등록</a>
								</div>
							</div>
						</div>
					</div>
					<br />
				</div>
				<!-- Q&A 테이블 끝 -->
		
				<br />
				<br />
				<hr />
	
		
				<!-- 배송 및 환불 시작 -->
		
				<h3 id="t4">배송 및 환불</h3>
				<br />
				<br />
				<div class="ship_info" style="width: 100%; margin: 0 auto; font-size: 0.9em; color: #212529;">
					<div class="row_ship_info">
						<h5>배송정보</h5>
						모든 제품은 메이킹퍼니처 직원이 직접 배송하며 배송비는 고객 부담입니다. 지역, 제품 수량 또는 설치 여부에 따라 주문 시
						배송비를 미리 안내해드립니다. 기본 배송비 외에 사다리차, 엘레베이터, 주차비 사용료 등 추가 비용이 발생 시 고객님
						부담입니다.
					</div>
					<br />
		
		
					<div class="row_ship_info">
						<h5>A/S</h5>
						핸드메이드 제품으로 나뭇결, 무늬, 옹이의 형태나 파임, 칠과 색상이 다를 수 있고 약간의 흠집이나 찍힘(표면의 크랙 또는
						뜯김)이 있을 수 있으며, 100% 원목으로 기후변화에 따른 수축, 팽창으로 인한 휘어짐(상판), 갈라짐(마구리면)이
						발생할 수 있습니다. 이런 자연스러운 현상들은 교체 및 교환 대상이 아니며 무상 A/S 사유가 되지 않습니다. 구매 전 꼭
						참고해주시기 바랍니다.
					</div>
					<br />
		
		
					<div class="row_ship_info">
						<h5>Wood Furniture</h5>
						원목가구의 수축 및 팽창은 원목만의 자연스러운 특징입니다.<br /> -지나치게 건조한 곳, 습한 곳은 피하세요. -가구의
						수평을 유지해주세요.(수평이 안 맞게 되면 가구가 뒤틀려버립니다.) -가구에 화학약품(신나, 메니큐어 등)이 닿지 않게
						해주세요. -평소에는 물걸레 청소하시면 됩니다. -뜨거운 냄비와 같은 고온의 뜨거운 물체에 직접 닿는 것은 피하시기
						바랍니다. -한 달에 1~2번 가구용 왁스로 닦아주세요. -원목가구는 수축기와 팽창기(계절의 변화)에 따라 갈라짐이나
						미세한 크랙이 발생할 수 있으며, 시간이 경과하면 원상회복이 될 수 있습니다. 이는 원목가구만의 자연스러운 현상이며 제품의
						하자가 아닙니다.
					</div>
					<br />
		
					<div class="row_ship_info">
						<h5>A/S규정[1년 무상]</h5>
						무상 A/S -보증기간 이내의 제품으로 정상적인 상태에서 제조상의 결함 제품 -원/부나내의 결함이 발생된 경우
						-메이킹퍼니처의 귀책 사유로 인한 결함 발생 제품
					</div>
					<div class="row_ship_info">유상 A/S -고객의 취급 부주의 및 고의적 훼손 경우 -고객 임의 개조에 의한
						파손의 경우나 타 업체에서 수리해 변경된 경우 -유상 서비스 요금은 [수리비+부품비+출장비+기타실 비용] 등이 포함됩니다.
					</div>
				</div>
					
				</div>
				<div class="col-4">
					<div class="sticky2" style="border: 1px solid #EAEAEA;">
						<form name="goodsform" action="#" method="post">
							<div class="row pt-4 pl-4">
								<div class="col-3" >
									<img src="${pageContext.request.contextPath}/resources/images/Community/peko.png" width="95%" >
								</div><hr>
								<div class="col-9">
									<h3><%=vo.getPRODUCT_SHOPNAME()%></h3>
									<p><font size="2"><%=vo.getPRODUCT_TITLE()%></font></p>
								</div>
							</div>
							<div>
								<table class="table table-borderless">
									<thead>
									    <tr>
									    	<th scope="col">가격</th>
									    	<th scope="col"><%=vo.getPRODUCT_PRICE()%>원</th>
									    </tr>
									</thead>
									<tbody>
									    <tr>
									    	<th scope="row">배송비</th>
									    	<td><%=vo.getPRODUCT_SHIP_PRICE()%>원</td>
									    </tr>
									    <tr>
									    	<th scope="row">색상선택</th>
									    	<td>
										      	<select name="color" size="1">
													<option value="">선택</option>
													<c:forTokens var="color" items="<%=PRODUCT_COLOR %>"
														delims=",">
														<option value="${fn:trim(color)}">${fn:trim(color)}</option>
													</c:forTokens>
												</select>
									    	</td>
									    </tr>
									    <tr>
									    	<th scope="row">사이즈선택</th>
									    	<td>
										      	<select name="size" size="1">
													<option value="">선택</option>
													<c:forTokens var="size" items="<%=PRODUCT_SIZE %>" delims=",">
														<option value="${fn:trim(size)}">${fn:trim(size)}</option>
													</c:forTokens>
																						
													
												</select>
									    	</td>
									    </tr>
									    <tr>
									    	<th scope="row">옵션선택</th>
									    	<td>
										      	<select name="option" size="1">
													<option value="">선택</option>
													<c:forTokens var="option" items="<%=PRODUCT_OPTION %>" delims=",">
														<option value="${fn:trim(option)}">${fn:trim(option)}</option>
													</c:forTokens>
												</select>
									    	</td>
									    </tr>
									    <tr>
									    	<th scope="row">수량</th>
									    	<td>
									      		<input name="amount" type="text" style="text-align: right" value="1" size="4" /> 
												<a href="JavaScript:count_change(0)">▲</a> 
												<a href="JavaScript:count_change(1)">▼</a> 개
									    	</td>
									    </tr>
									    <tr>
									    	<th scope="row">총 합계금액</th>
									    	<td></td>
									    </tr>
									</tbody>
								</table>
							</div>
							<div class="btnArea text-center">
								<a href="classreservation.ac" class="btn btn-outline-dark btn-lg" role="button" aria-pressed="true">장바구니</a>
								<a href="#" class="btn btn-outline-dark btn-lg" role="button" aria-pressed="true">바로구매</a>
							</div>
							<br/>
						</form>
						
					</div>
				</div>
			</div>
			<br>
		</div>
		<!-- content end -->
			

		<!-- jQuery first, then Popper.js, then Bootstrap JS -->
		<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>


	<script type="text/javascript">
	
	function check() {
		var content = reviewform.REVIEW_CONTENT.value;
		var grade = reviewform.REVIEW_GRADE.value;
		if(content=="") {
			alert('후기 내용을 입력해주세요');
			return false;
		} else if(grade=="") {
			alert('평점을 입력해주세요');
			return false;
		} 
		
		return true;
	}	
	
	
	
	function date_format(format) {
		var year = format.getFullYear();
	    var month = format.getMonth() + 1;
	    if(month<10) {
	    	month = '0' + month;
	    }
	    var date = format.getDate();
	    if(date<10) {
	    	date = '0' + date;
	    }
	    var hour = format.getHours();
	    if(hour<10) {
	    	hour = '0' + hour;
	    }
	    var min = format.getMinutes();
	    if(min<10) {
	    	min = '0' + min;
	    }

		return year + "-" + month + "-" + date + " " + hour + ":" + min;
	}
	

	function handleImgFileSelect(e) {
		
		//이미지 정보들을 초기화
		sel_files = [];
		$('.imgs_wrap').empty();
		
		var files = e.target.files;
		var filesArr = Array.prototype.slice.call(files);
		
		var index = 0;
		filesArr.forEach(function(f) {
			if(!f.type.match("image.*")) {
				alert("확장자는 이미지 확장자만 가능합니다.");
				return;
			}
			
			sel_files.push(f);
			
			var reader = new FileReader();
			reader.onload = function(e) {
			//	var html = "CONTENT";
				var html="<a href=\"javascript:void(0);\" onclick=\"deleteImageAction("+index+")\" id=\"img_id_"+index+"\"><img src=\"" + e.target.result + "\" data-file='"+f.name+"' class='selProductFile' title='Click to remove' width='100' height='100'></a>";
				
				$(".imgs_wrap").append(html);
				index++;
			}
			reader.readAsDataURL(f);
			
		});
	}		
	
	//이미지 정보들을 담을 배열
	var sel_files= [];
	
	$(document).ready(function(){
		$(".review_img").click(function(){
			if($(this).css('width') != '100px' ) {
				$(this).css({'width':'100','height':'100'});
			} else {
				$(this).css({'width':'100%','height':'100%'});
			}
			
		})
		
		$(".review_add").click(function() {
			if($(this).html() == '댓글 달기') { //댓글달기이면 hidden구역 보이고 버튼은 닫기로 바뀜
				$(".review_hidden").css('display','block');
				$(this).html('닫기');				
			} else { //버튼이 닫기이면 hidden 구역이 닫히고 버튼은 댓글 달기로 바뀜
				$(this).html('댓글 달기');
				$(".review_hidden").css('display','none');
			}			
			

		})
		
		//////////////////다중이미지 업로드
		$('#input_imgs').on("change", handleImgFileSelect);
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		$("#uploadBtn").on("click",function(e) {
			alert('a');
			var formData = new FormData();
			var inputFile = $("input[name='uploadFile']");
			var files = inputFile[0].files;

			for(var i = 0; i < files.length; i++) {
				formData.append("uploadFile", files[i]);
			}
			
			$.ajax({
				url : '/NAGAGU/insertreview1.st',
				processData : false,
				contentType : false,
				data : formData,
				type : 'POST',
				success:function(result) {		
					alert('Insert Success');
				}
			});
			
		});
		
		
		
		
		
		
		
		
		function selectQna() {	
			$('#output').empty();
			
			$.ajax({
				url:'/NAGAGU/getqna.st',
				type:'POST',
				data : {'QNA_PRODUCT':<%=PRODUCT_NUM %>},
				tadaType:"json",
				contentType:'application/x-www-form-urlencoded; charset=utf-8',
				success:function(data) {
					$.each(data, function(index, item) {
						var output = '';
						var QNA_DATE = new Date(item.QNA_DATE);
						var date = date_format(QNA_DATE);
						output += '<div class="row">';
						output += '<div class="col-1 justify-content-end">';
						output += '<img src="${pageContext.request.contextPath}/resources/images/Community/peko.png" alt="" class="img-circle">';
						output += '<div class="col-11">';
						output += '<div class="row">';
						output += '<div class="col-10 justify-content-end name">'+item.QNA_MEMBER+'</div>';
						output += '<div class="col-2 justify-content-center smallfont">'+QNA_DATE+'</div>';
						output += '</div>';
						output += '<div class="row ">';
						output += '<div class="col rep_content">'+item.QNA_CONTENT+'</div>';
						output += '</div>';
						output += '<div class="row" style="height: 20px;">';
						output += '<a href="#" class="smallfont">답글달기</a> &nbsp;&nbsp;';
						output += '<a href="#"	class="smallfont">신고하기</a>';
						output += '</div>';
						output += '</div>';
						output += '</div>';
						output += '</div>';
						console.log("output:" + output);
						$('#qna_sum').prepend(output);
					});					
					
				},
				error:function() {
					alert("ajax통신 실패!!!");
				}
			});
		}		
		
		
		
		$('#insert_comment').click(function(event) {
			if(check()) {
				
				alert('formData'+formData);
				alert("왔음");
				
				var formData = new FormData();
				var inputFile = $("input[name='input_imgs']");
				var files = inputFile[0].files;
				console.log(files);
				
				//add filedate to formdata
				for(var i = 0; i < files.length; i++) {
					formData.append("reviewform", files[i]);
				}
				
				alert('formData'+formData);
				alert("왔음");

				$.ajax({
					url : '/NAGAGU/insertreview.st', 
					processData : false,
					contentType : false,
					data : formData,
					type : 'POST',
					success:function(retVal) {				
						if(retVal.res == "OK") {
							alert('Insert Success');
						}
						else {
							alert("Insert Fail!!!");
						}
					},
					error:function(request, error) {
						alert("message:"+request.responseText);
					}
			});
			}
			event.preventDefault();
		});	
		
		
	});
	
	


	
	
	
	
	

	</script>








	</body>
</html>
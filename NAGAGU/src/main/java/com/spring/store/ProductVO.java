package com.spring.store;

import java.util.Date;

public class ProductVO {
	private int PRODUCT_NUM;
	private int PRODUCT_WORKSHOP;
	private String PRODUCT_SHOPNAME;
	private Date PRODUCT_DATE;
	private String PRODUCT_TITLE;
	private String PRODUCT_CATEGORY;
	private String PRODUCT_PIC;
	private int PRODUCT_PIC_COUNT;
	private int PRODUCT_PRICE;
	private String PRODUCT_GRADE;	
	private String PRODUCT_COLOR;	
	private int PRODUCT_READ;	
	private int PRODUCT_SALES;	
	private int PRODUCT_LIKE;
	private String PRODUCT_OPTION;	
	private String PRODUCT_SIZE;	
	private int PRODUCT_SHIP_PRICE;
	private String PRODUCT_SHIP_COMPANY;	
	private int PRODUCT_SHIP_RETURN_PRICE;
	private int PRODUCT_SHIP_CHANGE_PRICE;
	private String PRODUCT_SHIP_RETURN_PLACE;
	private String PRODUCT_SHIP_DAYS;
	public int getPRODUCT_NUM() {
		return PRODUCT_NUM;
	}
	public void setPRODUCT_NUM(int pRODUCT_NUM) {
		PRODUCT_NUM = pRODUCT_NUM;
	}
	public int getPRODUCT_WORKSHOP() {
		return PRODUCT_WORKSHOP;
	}
	public void setPRODUCT_WORKSHOP(int pRODUCT_WORKSHOP) {
		PRODUCT_WORKSHOP = pRODUCT_WORKSHOP;
	}
	public String getPRODUCT_SHOPNAME() {
		return PRODUCT_SHOPNAME;
	}
	public void setPRODUCT_SHOPNAME(String pRODUCT_SHOPNAME) {
		PRODUCT_SHOPNAME = pRODUCT_SHOPNAME;
	}
	public Date getPRODUCT_DATE() {
		return PRODUCT_DATE;
	}
	public void setPRODUCT_DATE(Date pRODUCT_DATE) {
		PRODUCT_DATE = pRODUCT_DATE;
	}
	public String getPRODUCT_TITLE() {
		return PRODUCT_TITLE;
	}
	public void setPRODUCT_TITLE(String pRODUCT_TITLE) {
		PRODUCT_TITLE = pRODUCT_TITLE;
	}
	public String getPRODUCT_CATEGORY() {
		return PRODUCT_CATEGORY;
	}
	public void setPRODUCT_CATEGORY(String pRODUCT_CATEGORY) {
		PRODUCT_CATEGORY = pRODUCT_CATEGORY;
	}
	public String getPRODUCT_PIC() {
		return PRODUCT_PIC;
	}
	public void setPRODUCT_PIC(String pRODUCT_PIC) {
		PRODUCT_PIC = pRODUCT_PIC;
	}
	public int getPRODUCT_PIC_COUNT() {
		return PRODUCT_PIC_COUNT;
	}
	public void setPRODUCT_PIC_COUNT(int pRODUCT_PIC_COUNT) {
		PRODUCT_PIC_COUNT = pRODUCT_PIC_COUNT;
	}
	public int getPRODUCT_PRICE() {
		return PRODUCT_PRICE;
	}
	public void setPRODUCT_PRICE(int pRODUCT_PRICE) {
		PRODUCT_PRICE = pRODUCT_PRICE;
	}
	public String getPRODUCT_GRADE() {
		return PRODUCT_GRADE;
	}
	public void setPRODUCT_GRADE(String pRODUCT_GRADE) {
		PRODUCT_GRADE = pRODUCT_GRADE;
	}
	public String getPRODUCT_COLOR() {
		return PRODUCT_COLOR;
	}
	public void setPRODUCT_COLOR(String pRODUCT_COLOR) {
		PRODUCT_COLOR = pRODUCT_COLOR;
	}
	public int getPRODUCT_READ() {
		return PRODUCT_READ;
	}
	public void setPRODUCT_READ(int pRODUCT_READ) {
		PRODUCT_READ = pRODUCT_READ;
	}
	public int getPRODUCT_SALES() {
		return PRODUCT_SALES;
	}
	public void setPRODUCT_SALES(int pRODUCT_SALES) {
		PRODUCT_SALES = pRODUCT_SALES;
	}
	public int getPRODUCT_LIKE() {
		return PRODUCT_LIKE;
	}
	public void setPRODUCT_LIKE(int pRODUCT_LIKE) {
		PRODUCT_LIKE = pRODUCT_LIKE;
	}
	public String getPRODUCT_OPTION() {
		return PRODUCT_OPTION;
	}
	public void setPRODUCT_OPTION(String pRODUCT_OPTION) {
		PRODUCT_OPTION = pRODUCT_OPTION;
	}
	public String getPRODUCT_SIZE() {
		return PRODUCT_SIZE;
	}
	public void setPRODUCT_SIZE(String pRODUCT_SIZE) {
		PRODUCT_SIZE = pRODUCT_SIZE;
	}
	public int getPRODUCT_SHIP_PRICE() {
		return PRODUCT_SHIP_PRICE;
	}
	public void setPRODUCT_SHIP_PRICE(int pRODUCT_SHIP_PRICE) {
		PRODUCT_SHIP_PRICE = pRODUCT_SHIP_PRICE;
	}
	public String getPRODUCT_SHIP_COMPANY() {
		return PRODUCT_SHIP_COMPANY;
	}
	public void setPRODUCT_SHIP_COMPANY(String pRODUCT_SHIP_COMPANY) {
		PRODUCT_SHIP_COMPANY = pRODUCT_SHIP_COMPANY;
	}
	public int getPRODUCT_SHIP_RETURN_PRICE() {
		return PRODUCT_SHIP_RETURN_PRICE;
	}
	public void setPRODUCT_SHIP_RETURN_PRICE(int pRODUCT_SHIP_RETURN_PRICE) {
		PRODUCT_SHIP_RETURN_PRICE = pRODUCT_SHIP_RETURN_PRICE;
	}
	public int getPRODUCT_SHIP_CHANGE_PRICE() {
		return PRODUCT_SHIP_CHANGE_PRICE;
	}
	public void setPRODUCT_SHIP_CHANGE_PRICE(int pRODUCT_SHIP_CHANGE_PRICE) {
		PRODUCT_SHIP_CHANGE_PRICE = pRODUCT_SHIP_CHANGE_PRICE;
	}
	public String getPRODUCT_SHIP_RETURN_PLACE() {
		return PRODUCT_SHIP_RETURN_PLACE;
	}
	public void setPRODUCT_SHIP_RETURN_PLACE(String pRODUCT_SHIP_RETURN_PLACE) {
		PRODUCT_SHIP_RETURN_PLACE = pRODUCT_SHIP_RETURN_PLACE;
	}
	public String getPRODUCT_SHIP_DAYS() {
		return PRODUCT_SHIP_DAYS;
	}
	public void setPRODUCT_SHIP_DAYS(String pRODUCT_SHIP_DAYS) {
		PRODUCT_SHIP_DAYS = pRODUCT_SHIP_DAYS;
	}
	
	
	
	
	
}

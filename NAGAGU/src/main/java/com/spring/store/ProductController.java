package com.spring.store;

import java.io.File;
import java.io.PrintWriter;
import java.net.URLEncoder;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

@Controller
public class ProductController {
	
	@Autowired
	private ProductService productService;
	private ProductReviewService reviewService;
	private QnaService qnaService;
	
	//의자 테이블 침대 협탁...
	@RequestMapping(value = "/productcategory.st")
	public String productcategory(Model model) {
		
		return "Store/productCategory";
	}
	

	//카테고리-의자만 받아와서 의자만 리스트 출력
	@RequestMapping(value = "/productlist.st", method = RequestMethod.GET)
	public String productlist(ProductVO productVO, Model model, HttpServletRequest request) {
		System.out.println("/productlist.st------------------------------");
		
		int page = 1; //초기값 1
		int limit = 9; //한 페이지당 출력할 글의 수
		
		if(request.getParameter("page") != null) {
			page = Integer.parseInt(request.getParameter("page"));
		}
		
		int startrow = (page - 1) * 9 + 1; // 읽기 시작할 row 번호.
		int endrow = startrow + limit - 1; //읽을 마지막 row 번호.
		
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("startrow", startrow);
		map.put("endrow", endrow);
		

		String PRODUCT_CATEGORY = productVO.getPRODUCT_CATEGORY();
		String sort = "new";
		if(request.getParameter("sort") != null) {
			sort = request.getParameter("sort");
		}

		map.put("PRODUCT_CATEGORY", PRODUCT_CATEGORY);
		map.put("sort", sort);
 
		
		int productcount;		
		ArrayList<ProductVO> productList = null;

		
		productcount = productService.getproductcount(map);
		productList = productService.getproductlist(map);			
		
		
		//총 페이지 수
		int maxpage = (int)((double)productcount / limit + 0.95); // 0.95를 더해서 올림 처리
		int startpage = (((int) ((double)page / 10 + 0.9)) - 1) * 10 + 1; // 현재 페이지에 보여줄 시작 페이지 수 (1, 11, 21 등...)
		int endpage =startpage + 10 - 1; // 현재 페이지에 보여줄 마지막 페이지 수 (10, 20, 30 등..)
		
		if (endpage > maxpage)
			endpage = maxpage;
		

		model.addAttribute("PRODUCT_CATEGORY", PRODUCT_CATEGORY);
		model.addAttribute("sort", sort);
		model.addAttribute("page", page);
		model.addAttribute("maxpage", maxpage);
		model.addAttribute("startpage", startpage);
		model.addAttribute("endpage", endpage);
		model.addAttribute("productcount", productcount);
		model.addAttribute("productList", productList);
		
		
		return "Store/productList";
	}


	
	@RequestMapping(value = "/productdetail.st", method = RequestMethod.GET)
	public String productdetail(ProductVO productVO, Model model, HttpServletRequest request) {
		
		String PRODUCT_CATEGORY = request.getParameter("PRODUCT_CATEGORY");
		int PRODUCT_NUM = Integer.parseInt(request.getParameter("PRODUCT_NUM"));
		int page = Integer.parseInt(request.getParameter("page"));
		
		
		ProductVO vo = productService.getproductVO(PRODUCT_NUM);
		
		
		
		System.out.println("vocolor="+vo.getPRODUCT_COLOR());
		model.addAttribute("productVO", vo);
		model.addAttribute("PRODUCT_CATEGORY", PRODUCT_CATEGORY);
		model.addAttribute("PRODUCT_NUM", PRODUCT_NUM);
		model.addAttribute("page", page);
		
		
		return "Store/productDetail";
	}
	
	/*에이젝스?!?!???*/
	
	@RequestMapping(value = "/insertreview.st", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> insertreview(@RequestParam("REVIEW_GRADE") int REVIEW_GRADE,@RequestParam("REVIEW_CONTENT") String REVIEW_CONTENT, @RequestParam("REVIEW_FILE") MultipartFile file, MultipartHttpServletRequest request, HttpServletResponse response) throws Exception {
		System.out.println("왔다");
		Map<String, Object> retVal = new HashMap<String, Object>();
		
		
		response.setCharacterEncoding("utf-8");
		response.setContentType("text/html; charset=utf-8"); 
		PrintWriter writer = response.getWriter();
		String downlink = null;		
		MultipartFile mf = request.getFile("file");

		if(!mf.isEmpty()) {
			String uploadPath = "C:\\Project138\\upload\\";
			String originalFileExtension = mf.getOriginalFilename().substring(mf.getOriginalFilename().lastIndexOf("."));
			String storedFileName = UUID.randomUUID().toString().replaceAll("-", "") + originalFileExtension;
			
			if(mf.getSize() != 0) {
				mf.transferTo(new File(uploadPath+storedFileName));
			}
			
			downlink = "fileDownload.do?of="+ URLEncoder.encode(storedFileName, "utf-8")
						+ "&of2=" + URLEncoder.encode(mf.getOriginalFilename(), "utf-8");
		} else {
			downlink = "첨부파일 없음";
		}
		
		Product_reviewVO reviewVO = new Product_reviewVO();
		reviewVO.setREVIEW_CONTENT(request.getParameter("REVIEW_CONTENT"));
		reviewVO.setREVIEW_DATE(new Timestamp(System.currentTimeMillis()));
		reviewVO.setREVIEW_FILE(mf.getOriginalFilename());
		reviewVO.setREVIEW_GRADE(Integer.parseInt(request.getParameter("REVIEW_GRADE")));
		reviewVO.setREVIEW_MEMBER(0);	//회원번호는 멤버 테이블에서 가져와야함
		reviewVO.setREVIEW_PRODUCT(Integer.parseInt(request.getParameter("REVIEW_PRODUCT")));
		
//
//		int x = reviewService.insertreview(reviewVO);
//		
//		if(x == 0) {
//			writer.write("<script>alert('글작성 실패');location.href='./writeform.do';</script>");
//		} else {
//			writer.write("<script>location.href='./list.do';</script>");
//		}		
//		
//		
		
//		try {
//			int res = ProductReviewMapperreviewService.insertreview(reviewVO);
//			
//			retVal.put("res", "OK");
//		} catch(Exception e) {
//			retVal.put("res", "Fail");
//			retVal.put("message","Failure");
//		}
		return retVal;
		
	}
	
	
	
	
	@RequestMapping(value = "/insertreview1.st", method = RequestMethod.POST)
	@ResponseBody
	public void insertreview1(MultipartFile[] uploadFile) throws Exception {
		System.out.println("왔다");

		String uploadFolder = "C:\\Project138\\upload\\";
		
		for(MultipartFile multipartFile : uploadFile) {
			String uploadFileName = multipartFile.getOriginalFilename();
			
			//
			uploadFileName  = uploadFileName.substring(uploadFileName.lastIndexOf("\\")+1);
			
			File saverFile = new File(uploadFolder, uploadFileName);
			
			try {
				multipartFile.transferTo(saverFile);
			} catch(Exception e) {
				
			}
			
		}
		
		
		
	}	
	
	
	
	
	
	
	
	
	
	
	@RequestMapping(value = "/getqna.st", method = RequestMethod.GET)
	public String getqna(HttpServletRequest request, Model model) {
		int QNA_PRODUCT = Integer.parseInt(request.getParameter("QNA_PRODUCT"));
		ArrayList<Product_qnaVO> qnaVO = qnaService.getqna(QNA_PRODUCT);
		
		return "Store/estimateList";
	}
	
	@RequestMapping(value = "/estimate.st", method = RequestMethod.GET)
	public String estimate(Model model) {
		
		
		return "Store/estimateList";
	}
	
	@RequestMapping(value = "/estimatedetail.st", method = RequestMethod.GET)
	public String estimatedetail(Model model) {
		
		
		return "Store/estimateDetail";
	}
	
	@RequestMapping(value = "/store_estimateform.st", method = RequestMethod.GET)
	public String store_estimateform(Model model) {
		
		
		return "Store/estimateForm";
	}
	
	@RequestMapping(value = "/store_estimateform_input.st", method = RequestMethod.POST)
	public String es_requestform_input(Model model) {
	
		return "Store/estimateList";
	}
		
}

package com.spring.academy;

import java.util.ArrayList;

public interface AcademyService {
	public ArrayList<ClassVO> getCls() throws Exception;
	public void insertClass(ClassVO academy) throws Exception;
	public int getCount() throws Exception;
}

package com.spring.store;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.mapper.ProductReviewMapper;

@Service
public class ProductReviewServiceImpl implements ProductReviewService {
	
	@Autowired
	private SqlSession sqlSession;
	
	@Override
	public int insertreview(Product_reviewVO reviewVO) {
		ProductReviewMapper productReviewMapper = sqlSession.getMapper(ProductReviewMapper.class);
		int res = productReviewMapper.insertreview(reviewVO);
		return res;
	}

}

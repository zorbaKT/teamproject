<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
		<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
		<style>
			.bg {
				border-bottom: 4px solid #ef900e;
			}
			
			@font-face {
				font-family: 'KOMACON';
				src:
					url('https://cdn.jsdelivr.net/gh/projectnoonnu/noonfonts_seven@1.2/KOMACON.woff')
					format('woff');
				font-weight: normal;
				font-style: normal;
			}
			
			.order-body {
				font-family: '만화진흥원체', 'KOMACON', KOMACON;
				font-size: 15px;
			}
			.dropdown-menu .dropdown-item a{
	            text-decoration: none;
	            color: black; 
	         }
			.bg {
				background-color: #1B1B27;
			}
			.header_util>ul>li {
				float: left;
				display: inline-block;
				vertical-align: top;
				line-height: 25px;
				padding: 8px 9px 0 10px;
				font-size: 13px;
				position: relative;
				z-index: 72;
			}
			
			.header_util>ul>li>a {
				color: #ffffff;
				text-decoration: none;
			}
			
			.header_util>ul>li>div>img {
				color: #ffffff;
				text-decoration: none;
			}
			
			.header_util>ul>li a:hover {
				opacity: 0.7;
			}
			
			.header_util>ul>li>div img:hover {
				opacity: 0.7;
			}
			
			.test {
				font-size: 17px;
			}
			
			.icons-btn {
				display: inline-block;
				text-align: center;
			}
			
			.icons-btn span {
				display: block;
				height: 40px;
				width: 40px;
				line-height: 40px;
			}
			
			@media ( max-width : 991.98px) {
				.icons-btn span {
					width: 24px;
				}
			}
			
			.icon-close2:before {
				content: "\e5cd";
			}
			
			.search-wrap {
				position: absolute;
				height: 50%;
				top: 0;
				left: 0;
				right: 0;
				bottom: 0;
				background: #fff;
				z-index: 999;
				opacity: 0;
				visibility: hidden;
				-webkit-transition: .5s all ease;
				-o-transition: .5s all ease;
				transition: .5s all ease;
			}
			
			.search-wrap.active {
				opacity: 1;
				visibility: visible;
			}
			
			.search-wrap .form-control {
				position: absolute;
				top: 80%;
				width: 100%;
				-webkit-transform: translateY(-50%);
				-ms-transform: translateY(-50%);
				transform: translateY(-50%);
				border: none;
				z-index: 3;
				font-size: 40px;
			}
			
			@media ( max-width : 991.98px) {
				.search-wrap .form-control {
					font-size: 20px;
				}
			}
			
			.search-wrap .search-close {
				z-index: 4;
				position: absolute;
				right: 20px;
				top: 100%;
				-webkit-transform: translateY(-50%);
				-ms-transform: translateY(-50%);
				transform: translateY(-50%);
			}
			
			.search-wrap .search-close span {
				font-size: 30px;
			}
		</style>
		<script>
			jQuery(document).ready(function($) {
				var searchShow = function() {
					// alert();
					var searchWrap = $('.search-wrap');
					$('.js-search-open').on('click', function(e) {
						e.preventDefault();
						searchWrap.addClass('active');
						setTimeout(function() {
							searchWrap.find('.form-control').focus();
						}, 300);
					});
					$('.js-search-close').on('click', function(e) {
						e.preventDefault();
						searchWrap.removeClass('active');
					})
				};
				searchShow();
			});
		</script>
	</head>
	
	<body>
		<div class="bg" >
			<div class="container">
				<div class="row">
					<nav class="navbar navbar-expand-lg navbar-dark w-100">
						<!-- Navbar content -->
						<a class="navbar-brand" href="./index.ma">
							<img src="${pageContext.request.contextPath}/resources/images/Main/NAGAGU2.png">
						</a>
						<button class="navbar-toggler" type="button" data-toggle="collapse"
							data-target="#navbarSupportedContent"
							aria-controls="navbarSupportedContent" aria-expanded="false"
							aria-label="Toggle navigation">
								<span class="navbar-toggler-icon"></span>
						</button>
						<div class="search-wrap">
							<a href="#" class="search-close js-search-close"> 
								<i class="fas fa-times"></i>
							</a>
							<form action="./search.my" method="post">
								<input type="text" class="form-control" placeholder="Search keyword and hit enter..." height="600px">
							</form>
						</div>
						<div class="collapse navbar-collapse" id="navbarSupportedContent">
							<div class="row w-50" style="padding-left: 5%;">
								<div class="test">
									<ul class="navbar-nav mr-auto">
										<li class="nav-item">
											<a class="nav-link" href="./community.cm">COMMUNITY
											<span class="sr-only">(current)</span>
										</a></li>
										<li class="nav-item">
											<a class="nav-link" href="./classlist.ac">ACADEMY</a>
										</li>
										<li class="nav-item dropdown">
											<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" 
											data-toggle="dropdown" aria-haspopup="true"	aria-expanded="false"> STORE </a>
											<div class="dropdown-menu" aria-labelledby="navbarDropdown">
												<a class="dropdown-item" href="./productcategory.st">수제가구</a> 
												<a class="dropdown-item" href="./estimate.st">견적문의</a>
												<!-- <div class="dropdown-divider"></div> -->
											</div>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="header_util">
							<ul>
								<li>
									<a href="#"	class="icons-btn d-inline-block js-search-open"> 
										<i class="fas fa-search" style="width: 20px; height: 25px;"></i>
									</a>
								</li>
								<li>
									<div class="dropdown">
										<img src="${pageContext.request.contextPath}/resources/images/Main/top_icon_mypage.png" alt=""
											id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" />
										<div class="dropdown-menu" aria-labelledby="dropdownMenu2">
											<button class="dropdown-item" type="button"><a href="./login.ma">로그인</a></button>
											<button class="dropdown-item" type="button"><a href="./signup.ma">회원가입</a></button>
											<button class="dropdown-item" type="button"><a href="./order_detail.my">주문조회</a></button>
											<button class="dropdown-item" type="button"><a href="./mypage_edit.my">내정보수정</a></button>
											<button class="dropdown-item" type="button"><a href="./mypage_like.my">좋아요</a></button>
											<button class="dropdown-item" type="button"><a href="./workshop.no">공방관리</a></button>
										</div>
									</div>
								</li>
								<li>
									<a href="./mypage_basket.my">
										<img src="${pageContext.request.contextPath}/resources/images/Main/top_icon_cart.png" alt="" />
									</a>
								</li>
								<li><a href="./mypage.my">MYPAGE</a></li>
							</ul>
						</div>
					</nav>
				</div>
			</div>
		</div>
		<script src="https://kit.fontawesome.com/b74b42490f.js" crossorigin="anonymous"></script>
		<script src="<c:url value="/resources/js/header.js"/>"></script>
	</body>
</html>
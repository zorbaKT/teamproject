package com.spring.academy;

import java.sql.Date;

import org.springframework.web.multipart.MultipartFile;

public class ClassVO {
	private int CLASS_NUMBER;
	private String CLASS_DIVISION;
	private String CLASS_NAME;
	private String CLASS_ABRIEF;
	private int CLASS_AMOUNT;
	private Date CLASS_DATE;
	private String CLASS_AREA;
	private String CLASS_CATEGORY;
	private String CLASS_INTRODUCTION;
	private String CLASS_BANNER;
	private String CLASS_IMAGE;
	private String CLASS_ADDRESS;
	
	public int getCLASS_NUMBER() {
		return CLASS_NUMBER;
	}
	public void setCLASS_NUMBER(int cLASS_NUMBER) {
		CLASS_NUMBER = cLASS_NUMBER;
	}
	public String getCLASS_DIVISION() {
		return CLASS_DIVISION;
	}
	public void setCLASS_DIVISION(String cLASS_DIVISION) {
		CLASS_DIVISION = cLASS_DIVISION;
	}
	public String getCLASS_NAME() {
		return CLASS_NAME;
	}
	public void setCLASS_NAME(String cLASS_NAME) {
		CLASS_NAME = cLASS_NAME;
	}
	public String getCLASS_ABRIEF() {
		return CLASS_ABRIEF;
	}
	public void setCLASS_ABRIEF(String cLASS_ABRIEF) {
		CLASS_ABRIEF = cLASS_ABRIEF;
	}
	public int getCLASS_AMOUNT() {
		return CLASS_AMOUNT;
	}
	public void setCLASS_AMOUNT(int cLASS_AMOUNT) {
		CLASS_AMOUNT = cLASS_AMOUNT;
	}
	public Date getCLASS_DATE() {
		return CLASS_DATE;
	}
	public void setCLASS_DATE(Date cLASS_DATE) {
		CLASS_DATE = cLASS_DATE;
	}
	public String getCLASS_AREA() {
		return CLASS_AREA;
	}
	public void setCLASS_AREA(String cLASS_AREA) {
		CLASS_AREA = cLASS_AREA;
	}
	public String getCLASS_CATEGORY() {
		return CLASS_CATEGORY;
	}
	public void setCLASS_CATEGORY(String cLASS_CATEGORY) {
		CLASS_CATEGORY = cLASS_CATEGORY;
	}
	public String getCLASS_INTRODUCTION() {
		return CLASS_INTRODUCTION;
	}
	public void setCLASS_INTRODUCTION(String cLASS_INTRODUCTION) {
		CLASS_INTRODUCTION = cLASS_INTRODUCTION;
	}
	public String getCLASS_ADDRESS() {
		return CLASS_ADDRESS;
	}
	public void setCLASS_ADDRESS(String cLASS_ADDRESS) {
		CLASS_ADDRESS = cLASS_ADDRESS;
	}
	public String getCLASS_BANNER() {
		return CLASS_BANNER;
	}
	public void setCLASS_BANNER(String cLASS_BANNER) {
		CLASS_BANNER = cLASS_BANNER;
	}
	public String getCLASS_IMAGE() {
		return CLASS_IMAGE;
	}
	public void setCLASS_IMAGE(String cLASS_IMAGE) {
		CLASS_IMAGE = cLASS_IMAGE;
	}
}

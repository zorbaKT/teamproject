<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.*,com.spring.academy.*" %>
<%
	List<ClassVO> classList = (ArrayList<ClassVO>)request.getAttribute("classList");
	int listcount = ((Integer)request.getAttribute("listcount")).intValue(); // 글개수
%>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/Academy/classlist_.css">
		<style>
			@font-face {
				font-family: 'KOMACON';
				src:
					url('https://cdn.jsdelivr.net/gh/projectnoonnu/noonfonts_seven@1.2/KOMACON.woff')
					format('woff');
				font-weight: normal;
				font-style: normal;
			}
			
			.order-body {
				font-family: '만화진흥원체', 'KOMACON', KOMACON;
				font-size: 15px;
			}
			
			a.nav-link.active {
				background-color: #1B1B27 !important;
				color: white !important;
			}
			
			.pagination {
			  display: inline-block;
			  margin-bottom:30px;
			}
			.pagination a {
			  color: black;
			  float: left;
			  padding: 8px 16px;
			  text-decoration: none;
			}
			.pagination a:hover {
			  background-color: #ef902e;
			  color:black;
			}
		</style>

	</head>
	<body class="order-body">	
		
		<div class="container" id="class_container">
		
			<!-- best 1,2,3 순위 -->
			<div class="row row-cols-1 row-cols-md-3" id="lesson_list">			
				<div class="col mb-5" align="center">
					<div class="col">
					<i style="color: #FFDF00" class="fas fa-medal fa-2x"></i>
					<br>
					Best 1
				</div>
					<div class="card h-100">
						<a href="./classdetail.ac">
							<img src="https://cdn.pixabay.com/photo/2017/03/28/12/16/chairs-2181980_960_720.jpg" class="card-img-top" alt="...">
							<div class="card-body">
								<h5 class="card-title text-center">ZAZABTO</h5>
								<p class="card-text text-center">
									[텐텐클래스] (동작) 귀여운 진저맨 디저트 캔들 만들기
								</p>
								<p class="card-text text-center">55,000원</p>
							</div>
						</a>
					</div>
				</div>
				<div class="col mb-5" align="center">
					<div class="col" id="best2">
						<i style="color: #778899" class="fas fa-medal fa-2x"></i>
						<br>
						Best 2
					</div>
					<div class="card h-100">
						<img
							src="https://cdn.pixabay.com/photo/2017/03/01/05/12/tea-cup-2107599_960_720.jpg" class="card-img-top" alt="...">
						<div class="card-body">
							<a href="#"><h5 class="card-title text-center">ZAZABTO</h5></a>
							<p class="card-text text-center">
								[텐텐클래스] (동작) 귀여운 진저맨 디저트 캔들 만들기
							</p>
							<p class="card-text text-center">55,000원</p>
						</div>
					</div>
				</div>
				<div class="col mb-5" align="center">
					<div class="col" id="best3">
						<i style="color: #8b4513" class="fas fa-medal fa-2x"></i>
						<br>
						Best 3
					</div>
					<div class="card h-100">
						<img
							src="https://cdn.pixabay.com/photo/2017/08/01/12/43/kitchen-2565105_960_720.jpg" class="card-img-top" alt="...">
						<div class="card-body">
							<a href="#"><h5 class="card-title text-center">ZAZABTO</h5></a>
							<p class="card-text text-center">
								[텐텐클래스] (동작) 귀여운 진저맨 디저트 캔들 만들기
							</p>
							<p class="card-text text-center">55,000원</p>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				
			</div>
			<!-- 클래스 nav -->
			<div class="d-flex bd-highlight" style="padding: 2% 0;">
					<div  class="mr-auto bd-highlight">
						<table>
							<tr>
								<td>
									<select class="form-control">
										<option selected>지역</option>
										<option>종로구</option>
										<option>중구</option>
										<option>용산구</option>
										<option>성동구</option>
										<option>광진구</option>
										<option>동대문구</option>
										<option>중랑구</option>
										<option>성북구</option>
										<option>강북구</option>
										<option>도봉구</option>
										<option>노원구</option>
										<option>은평구</option>
										<option>서대문구</option>
										<option>마포구</option>
										<option>양천구</option>
										<option>강서구</option>
										<option>구로구</option>
										<option>금천구</option>
										<option>영등포구</option>
										<option>동작구</option>
										<option>관악구</option>
										<option>서초구</option>
										<option>강남구</option>
										<option>송파구</option>
										<option>강동구</option>
									</select>
								</td>
								<td>
									<select class="form-control">
										<option selected>분류</option>
										<option>좋아요</option>
										<option>최신</option>
									</select>
								</td>
							</tr>
						</table>
					</div>
					<div class="bd-highlight">
						<a href="./classform.ac" class="btn btn-outline-dark" role="button" aria-pressed="true">등록하기</a>
					</div>
				</div>
			<div class="nav">
				<ul class="nav nav-tabs mb-2 nav-fill" role="classTablist" id="nav-tab-bar">
					<li class="nav-item">
					    <a class="nav-link active" id="all-tab" data-toggle="tab" href="#class-all" role="tab"
					    aria-controls="class-all" aria-selected="false"> <span>전체</span></a>
					</li>
					<li class="nav-item">
					    <a class="nav-link" id="desk-tab" data-toggle="tab" href="#class-desk" role="tab" 
					    aria-controls="class-desk" aria-selected="true"> <span>책상</span></a>
					</li>
					<li class="nav-item">
					    <a class="nav-link" id="chair-tab" data-toggle="tab" href="#class-chair" role="tab"
					    aria-controls="class-chair" aria-selected="false"> <span>의자</span></a>
					</li>
					<li class="nav-item">
					    <a class="nav-link" id="bookshelf-tab" data-toggle="tab" href="#class-bookshelf" role="tab"
					    aria-controls="class-bookshelf" aria-selected="false"> <span>책장</span></a>
					</li>
					<li class="nav-item">
					    <a class="nav-link" id="bed-tab" data-toggle="tab" href="#class-bed" role="tab"
					    aria-controls="class-bed" aria-selected="false"> <span>침대</span></a>
					</li>
					<li class="nav-item">
					    <a class="nav-link" id="drawer-tab" data-toggle="tab" href="#class-drawer" role="tab"
					    aria-controls="class-drawer" aria-selected="false"> <span>서랍장</span></a>
					</li>
					<li class="nav-item">
					    <a class="nav-link" id="side-table-tab" data-toggle="tab" href="#class-side-table" role="tab"
					    aria-controls="class-side-table" aria-selected="false"> <span>협탁</span></a>
					</li>
					<li class="nav-item">
					    <a class="nav-link" id="dressing-table-tab" data-toggle="tab" href="#class-dressing-table" role="tab"
					    aria-controls="class-dressing-table" aria-selected="false"> <span>화장대</span></a>
					</li>
					<li class="nav-item">
					    <a class="nav-link" id="others-tab" data-toggle="tab" href="#class-others" role="tab"
					    aria-controls="class-others" aria-selected="false"> <span>기타</span></a>
					</li>
				</ul>
			</div>
			
			<div class="tab-content" id="classTabContent">
			
				<!-- 전체 메뉴 -->
				<div class="tab-pane fade show active row" id="class-all" role="tabpanel" aria-labelledby="home-tab">
					<!-- 작업들어간다잉 -->
					<%
						if(listcount > 0) {	
					%>
							<div class="row row-cols-1 row-cols-md-4" id="rank">		
					<%
							for(int i = 0; i < classList.size(); i++) {
								ClassVO cl = (ClassVO)classList.get(i); // 캐스트 연산 필수
					%>
					
							<div class="col mb-3">
								<div class="card h-100">
									<img src="<%=cl.getCLASS_IMAGE()%>"	class="card-img-top" alt="...">
									<div class="card-body">
										<a href="./classdetail.ac?CLASS_NUMBER=<%=cl.getCLASS_NUMBER()%>"><h5 class="card-title text-center"><%=cl.getCLASS_NAME()%></h5></a>
										<p class="card-text text-center">
											<%=cl.getCLASS_ABRIEF()%>
										</p>
										<p class="card-text text-center"><%=cl.getCLASS_AMOUNT()%>원</p>
									</div>
								</div>
							</div>
					<%
							}
					%>
							</div>
					<%
						} else {
					%>
						<div class="text-center">
							<p> 현재 진행중인 강의가 없습니다. </p>
						</div>
					<%
						}
					%>
					<!-- pagenation -->
					<div class="row justify-content-center">
						<div class="pagination">
						  <a href="#">&laquo;</a>
						  <a href="#">1</a>
						  <a href="#">2</a>
						  <a href="#">3</a>
						  <a href="#">4</a>
						  <a href="#">5</a>
						  <a href="#">6</a>
						  <a href="#">&raquo;</a>
						</div>
					</div>
				</div>
				
				<!-- 책상 tab -->
				<div class="tab-pane fade show row " id="class-desk" role="tabpanel" aria-labelledby="home-tab">
					<div class="row row-cols-1 row-cols-md-4" id="rank">
						<div class="col mb-3">
							<div class="card h-80">
								<img src="https://cdn.pixabay.com/photo/2015/02/09/14/56/table-629772_960_720.jpg"	class="card-img-top" alt="...">
								<div class="card-body">
									<a href="#"><h5 class="card-title text-center">ZAZABTO</h5></a>
									<p class="card-text text-center">
										[텐텐클래스] (동작) 귀여운 진저맨 디저트 캔들 만들기
									</p>
									<p class="card-text text-center">55,000원</p>
								</div>
							</div>
						</div>
						<div class="col mb-3">
							<div class="card h-80">
								<img src="${pageContext.request.contextPath}/resources/images/test.jpg"	class="card-img-top" alt="...">
								<div class="card-body">
									<a href="#"><h5 class="card-title text-center">ZAZABTO</h5></a>
									<p class="card-text text-center">
										[텐텐클래스] (동작) 귀여운 진저맨 디저트 캔들 만들기
									</p>
									<p class="card-text text-center">55,000원</p>
								</div>
							</div>
						</div>
						<div class="col mb-3">
							<div class="card h-80">
								<img
									src="${pageContext.request.contextPath}/resources/images/test.jpg" class="card-img-top" alt="...">
								<div class="card-body">
									<a href="#"><h5 class="card-title text-center">ZAZABTO</h5></a>
									<p class="card-text text-center">
										[텐텐클래스] (동작) 귀여운 진저맨 디저트 캔들 만들기
									</p>
									<p class="card-text text-center">55,000원</p>
								</div>
							</div>
						</div>
						<div class="col mb-3">
							<div class="card h-80">
								<img src="${pageContext.request.contextPath}/resources/images/test.jpg"	class="card-img-top" alt="...">
								<div class="card-body">
									<a href="#"><h5 class="card-title text-center">ZAZABTO</h5></a>
									<p class="card-text text-center">
										[텐텐클래스] (동작) 귀여운 진저맨 디저트 캔들 만들기
									</p>
									<p class="card-text text-center">55,000원</p>
								</div>
							</div>
						</div>
					</div>
					<!-- pagenation -->
					<div class="row justify-content-center">
						<div class="pagination">
						  <a href="#">&laquo;</a>
						  <a href="#">1</a>
						  <a href="#">2</a>
						  <a href="#">3</a>
						  <a href="#">4</a>
						  <a href="#">5</a>
						  <a href="#">6</a>
						  <a href="#">&raquo;</a>
						</div>
					</div>
				</div>
				
				<!-- 의자tab -->
				<div class="tab-pane fade show row " id="class-chair" role="tabpanel" aria-labelledby="home-tab">
					<div class="row row-cols-1 row-cols-md-4" id="rank">
						<div class="col mb-3">
							<div class="card h-80">
								<img src="https://cdn.pixabay.com/photo/2015/02/09/14/56/table-629772_960_720.jpg"	class="card-img-top" alt="...">
								<div class="card-body">
									<a href="#"><h5 class="card-title text-center">ZAZABTO</h5></a>
									<p class="card-text text-center">
										[텐텐클래스] (동작) 귀여운 진저맨 디저트 캔들 만들기
									</p>
									<p class="card-text text-center">55,000원</p>
								</div>
							</div>
						</div>
						<div class="col mb-3">
							<div class="card h-80">
								<img src="${pageContext.request.contextPath}/resources/images/test.jpg"	class="card-img-top" alt="...">
								<div class="card-body">
									<a href="#"><h5 class="card-title text-center">ZAZABTO</h5></a>
									<p class="card-text text-center">
										[텐텐클래스] (동작) 귀여운 진저맨 디저트 캔들 만들기
									</p>
									<p class="card-text text-center">55,000원</p>
								</div>
							</div>
						</div>
						<div class="col mb-3">
							<div class="card h-80">
								<img
									src="${pageContext.request.contextPath}/resources/images/test.jpg" class="card-img-top" alt="...">
								<div class="card-body">
									<a href="#"><h5 class="card-title text-center">ZAZABTO</h5></a>
									<p class="card-text text-center">
										[텐텐클래스] (동작) 귀여운 진저맨 디저트 캔들 만들기
									</p>
									<p class="card-text text-center">55,000원</p>
								</div>
							</div>
						</div>
						<div class="col mb-3">
							<div class="card h-80">
								<img src="${pageContext.request.contextPath}/resources/images/test.jpg"	class="card-img-top" alt="...">
								<div class="card-body">
									<a href="#"><h5 class="card-title text-center">ZAZABTO</h5></a>
									<p class="card-text text-center">
										[텐텐클래스] (동작) 귀여운 진저맨 디저트 캔들 만들기
									</p>
									<p class="card-text text-center">55,000원</p>
								</div>
							</div>
						</div>
					</div>
					<!-- pagenation -->
					<div class="row justify-content-center">
						<div class="pagination">
						  <a href="#">&laquo;</a>
						  <a href="#">1</a>
						  <a href="#">2</a>
						  <a href="#">3</a>
						  <a href="#">4</a>
						  <a href="#">5</a>
						  <a href="#">6</a>
						  <a href="#">&raquo;</a>
						</div>
					</div>
				</div>
				
				<!-- 책장tab -->
				<div class="tab-pane fade show row " id="class-bookshelf" role="tabpanel" aria-labelledby="home-tab">
					<div class="row row-cols-1 row-cols-md-4" id="rank">
						<div class="col mb-3">
							<div class="card h-80">
								<img src="https://cdn.pixabay.com/photo/2015/02/09/14/56/table-629772_960_720.jpg"	class="card-img-top" alt="...">
								<div class="card-body">
									<a href="#"><h5 class="card-title text-center">ZAZABTO</h5></a>
									<p class="card-text text-center">
										[텐텐클래스] (동작) 귀여운 진저맨 디저트 캔들 만들기
									</p>
									<p class="card-text text-center">55,000원</p>
								</div>
							</div>
						</div>
						<div class="col mb-3">
							<div class="card h-80">
								<img src="${pageContext.request.contextPath}/resources/images/test.jpg"	class="card-img-top" alt="...">
								<div class="card-body">
									<a href="#"><h5 class="card-title text-center">ZAZABTO</h5></a>
									<p class="card-text text-center">
										[텐텐클래스] (동작) 귀여운 진저맨 디저트 캔들 만들기
									</p>
									<p class="card-text text-center">55,000원</p>
								</div>
							</div>
						</div>
						<div class="col mb-3">
							<div class="card h-80">
								<img
									src="${pageContext.request.contextPath}/resources/images/test.jpg" class="card-img-top" alt="...">
								<div class="card-body">
									<a href="#"><h5 class="card-title text-center">ZAZABTO</h5></a>
									<p class="card-text text-center">
										[텐텐클래스] (동작) 귀여운 진저맨 디저트 캔들 만들기
									</p>
									<p class="card-text text-center">55,000원</p>
								</div>
							</div>
						</div>
					</div>
					<!-- pagenation -->
					<div class="row justify-content-center">
						<div class="pagination">
						  <a href="#">&laquo;</a>
						  <a href="#">1</a>
						  <a href="#">2</a>
						  <a href="#">3</a>
						  <a href="#">4</a>
						  <a href="#">5</a>
						  <a href="#">6</a>
						  <a href="#">&raquo;</a>
						</div>
					</div>
				</div>
				
				<!-- 침대tab -->
				<div class="tab-pane fade show row " id="class-bed" role="tabpanel" aria-labelledby="home-tab">
					<div class="row row-cols-1 row-cols-md-4" id="rank">
						<div class="col mb-3">
							<div class="card h-80">
								<img
									src="${pageContext.request.contextPath}/resources/images/test.jpg" class="card-img-top" alt="...">
								<div class="card-body">
									<a href="#"><h5 class="card-title text-center">ZAZABTO</h5></a>
									<p class="card-text text-center">
										[텐텐클래스] (동작) 귀여운 진저맨 디저트 캔들 만들기
									</p>
									<p class="card-text text-center">55,000원</p>
								</div>
							</div>
						</div>
						<div class="col mb-3">
							<div class="card h-80">
								<img src="${pageContext.request.contextPath}/resources/images/test.jpg"	class="card-img-top" alt="...">
								<div class="card-body">
									<a href="#"><h5 class="card-title text-center">ZAZABTO</h5></a>
									<p class="card-text text-center">
										[텐텐클래스] (동작) 귀여운 진저맨 디저트 캔들 만들기
									</p>
									<p class="card-text text-center">55,000원</p>
								</div>
							</div>
						</div>
					</div>
					<!-- pagenation -->
					<div class="row justify-content-center">
						<div class="pagination">
						  <a href="#">&laquo;</a>
						  <a href="#">1</a>
						  <a href="#">2</a>
						  <a href="#">3</a>
						  <a href="#">4</a>
						  <a href="#">5</a>
						  <a href="#">6</a>
						  <a href="#">&raquo;</a>
						</div>
					</div>
				</div>
				
				<!-- 서랍장tab -->
				<div class="tab-pane fade show row " id="class-drawer" role="tabpanel" aria-labelledby="home-tab">
					<div class="row row-cols-1 row-cols-md-4" id="rank">
						<div class="col mb-3">
							<div class="card h-80">
								<img src="https://cdn.pixabay.com/photo/2015/02/09/14/56/table-629772_960_720.jpg"	class="card-img-top" alt="...">
								<div class="card-body">
									<a href="#"><h5 class="card-title text-center">ZAZABTO</h5></a>
									<p class="card-text text-center">
										[텐텐클래스] (동작) 귀여운 진저맨 디저트 캔들 만들기
									</p>
									<p class="card-text text-center">55,000원</p>
								</div>
							</div>
						</div>
						<div class="col mb-3">
							<div class="card h-80">
								<img src="${pageContext.request.contextPath}/resources/images/test.jpg"	class="card-img-top" alt="...">
								<div class="card-body">
									<a href="#"><h5 class="card-title text-center">ZAZABTO</h5></a>
									<p class="card-text text-center">
										[텐텐클래스] (동작) 귀여운 진저맨 디저트 캔들 만들기
									</p>
									<p class="card-text text-center">55,000원</p>
								</div>
							</div>
						</div>
						<div class="col mb-3">
							<div class="card h-80">
								<img
									src="${pageContext.request.contextPath}/resources/images/test.jpg" class="card-img-top" alt="...">
								<div class="card-body">
									<a href="#"><h5 class="card-title text-center">ZAZABTO</h5></a>
									<p class="card-text text-center">
										[텐텐클래스] (동작) 귀여운 진저맨 디저트 캔들 만들기
									</p>
									<p class="card-text text-center">55,000원</p>
								</div>
							</div>
						</div>
						<div class="col mb-3">
							<div class="card h-80">
								<img src="${pageContext.request.contextPath}/resources/images/test.jpg"	class="card-img-top" alt="...">
								<div class="card-body">
									<a href="#"><h5 class="card-title text-center">ZAZABTO</h5></a>
									<p class="card-text text-center">
										[텐텐클래스] (동작) 귀여운 진저맨 디저트 캔들 만들기
									</p>
									<p class="card-text text-center">55,000원</p>
								</div>
							</div>
						</div>
					</div>
					<!-- pagenation -->
					<div class="row justify-content-center">
						<div class="pagination">
						  <a href="#">&laquo;</a>
						  <a href="#">1</a>
						  <a href="#">2</a>
						  <a href="#">3</a>
						  <a href="#">4</a>
						  <a href="#">5</a>
						  <a href="#">6</a>
						  <a href="#">&raquo;</a>
						</div>
					</div>
				</div>
				
				<!-- 협탁tab -->
				<div class="tab-pane fade show row " id="class-size-table" role="tabpanel" aria-labelledby="home-tab">
					<div class="row row-cols-1 row-cols-md-4" id="rank">
						<div class="col mb-3">
							<div class="card h-80">
								<img src="https://cdn.pixabay.com/photo/2015/02/09/14/56/table-629772_960_720.jpg"	class="card-img-top" alt="...">
								<div class="card-body">
									<a href="#"><h5 class="card-title text-center">ZAZABTO</h5></a>
									<p class="card-text text-center">
										[텐텐클래스] (동작) 귀여운 진저맨 디저트 캔들 만들기
									</p>
									<p class="card-text text-center">55,000원</p>
								</div>
							</div>
						</div>
						<div class="col mb-3">
							<div class="card h-80">
								<img src="${pageContext.request.contextPath}/resources/images/test.jpg"	class="card-img-top" alt="...">
								<div class="card-body">
									<a href="#"><h5 class="card-title text-center">ZAZABTO</h5></a>
									<p class="card-text text-center">
										[텐텐클래스] (동작) 귀여운 진저맨 디저트 캔들 만들기
									</p>
									<p class="card-text text-center">55,000원</p>
								</div>
							</div>
						</div>
						<div class="col mb-3">
							<div class="card h-80">
								<img
									src="${pageContext.request.contextPath}/resources/images/test.jpg" class="card-img-top" alt="...">
								<div class="card-body">
									<a href="#"><h5 class="card-title text-center">ZAZABTO</h5></a>
									<p class="card-text text-center">
										[텐텐클래스] (동작) 귀여운 진저맨 디저트 캔들 만들기
									</p>
									<p class="card-text text-center">55,000원</p>
								</div>
							</div>
						</div>
						<div class="col mb-3">
							<div class="card h-80">
								<img src="${pageContext.request.contextPath}/resources/images/test.jpg"	class="card-img-top" alt="...">
								<div class="card-body">
									<a href="#"><h5 class="card-title text-center">ZAZABTO</h5></a>
									<p class="card-text text-center">
										[텐텐클래스] (동작) 귀여운 진저맨 디저트 캔들 만들기
									</p>
									<p class="card-text text-center">55,000원</p>
								</div>
							</div>
						</div>
					</div>
					<!-- pagenation -->
					<div class="row justify-content-center">
						<div class="pagination">
						  <a href="#">&laquo;</a>
						  <a href="#">1</a>
						  <a href="#">2</a>
						  <a href="#">3</a>
						  <a href="#">4</a>
						  <a href="#">5</a>
						  <a href="#">6</a>
						  <a href="#">&raquo;</a>
						</div>
					</div>
				</div>
				
				<!-- 화장대tab -->
				<div class="tab-pane fade show row " id="class-dressing-table" role="tabpanel" aria-labelledby="home-tab">
					<div class="row row-cols-1 row-cols-md-4" id="rank">
						<div class="col mb-3">
							<div class="card h-80">
								<img src="https://cdn.pixabay.com/photo/2015/02/09/14/56/table-629772_960_720.jpg"	class="card-img-top" alt="...">
								<div class="card-body">
									<a href="#"><h5 class="card-title text-center">ZAZABTO</h5></a>
									<p class="card-text text-center">
										[텐텐클래스] (동작) 귀여운 진저맨 디저트 캔들 만들기
									</p>
									<p class="card-text text-center">55,000원</p>
								</div>
							</div>
						</div>
						<div class="col mb-3">
							<div class="card h-80">
								<img src="${pageContext.request.contextPath}/resources/images/test.jpg"	class="card-img-top" alt="...">
								<div class="card-body">
									<a href="#"><h5 class="card-title text-center">ZAZABTO</h5></a>
									<p class="card-text text-center">
										[텐텐클래스] (동작) 귀여운 진저맨 디저트 캔들 만들기
									</p>
									<p class="card-text text-center">55,000원</p>
								</div>
							</div>
						</div>
						<div class="col mb-3">
							<div class="card h-80">
								<img
									src="${pageContext.request.contextPath}/resources/images/test.jpg" class="card-img-top" alt="...">
								<div class="card-body">
									<a href="#"><h5 class="card-title text-center">ZAZABTO</h5></a>
									<p class="card-text text-center">
										[텐텐클래스] (동작) 귀여운 진저맨 디저트 캔들 만들기
									</p>
									<p class="card-text text-center">55,000원</p>
								</div>
							</div>
						</div>
						<div class="col mb-3">
							<div class="card h-80">
								<img src="${pageContext.request.contextPath}/resources/images/test.jpg"	class="card-img-top" alt="...">
								<div class="card-body">
									<a href="#"><h5 class="card-title text-center">ZAZABTO</h5></a>
									<p class="card-text text-center">
										[텐텐클래스] (동작) 귀여운 진저맨 디저트 캔들 만들기
									</p>
									<p class="card-text text-center">55,000원</p>
								</div>
							</div>
						</div>
					</div>
					<!-- pagenation -->
					<div class="row justify-content-center">
						<div class="pagination">
						  <a href="#">&laquo;</a>
						  <a href="#">1</a>
						  <a href="#">2</a>
						  <a href="#">3</a>
						  <a href="#">4</a>
						  <a href="#">5</a>
						  <a href="#">6</a>
						  <a href="#">&raquo;</a>
						</div>
					</div>
				</div>
				
				<!-- 기타tab -->
				<div class="tab-pane fade show row " id="class-others" role="tabpanel" aria-labelledby="home-tab">
					<div class="row row-cols-1 row-cols-md-4" id="rank">
						<div class="col mb-3">
							<div class="card h-80">
								<img src="https://cdn.pixabay.com/photo/2015/02/09/14/56/table-629772_960_720.jpg"	class="card-img-top" alt="...">
								<div class="card-body">
									<a href="#"><h5 class="card-title text-center">ZAZABTO</h5></a>
									<p class="card-text text-center">
										[텐텐클래스] (동작) 귀여운 진저맨 디저트 캔들 만들기
									</p>
									<p class="card-text text-center">55,000원</p>
								</div>
							</div>
						</div>
						<div class="col mb-3">
							<div class="card h-80">
								<img src="${pageContext.request.contextPath}/resources/images/test.jpg"	class="card-img-top" alt="...">
								<div class="card-body">
									<a href="#"><h5 class="card-title text-center">ZAZABTO</h5></a>
									<p class="card-text text-center">
										[텐텐클래스] (동작) 귀여운 진저맨 디저트 캔들 만들기
									</p>
									<p class="card-text text-center">55,000원</p>
								</div>
							</div>
						</div>
						<div class="col mb-3">
							<div class="card h-80">
								<img src="${pageContext.request.contextPath}/resources/images/test.jpg"	class="card-img-top" alt="...">
								<div class="card-body">
									<a href="#"><h5 class="card-title text-center">ZAZABTO</h5></a>
									<p class="card-text text-center">
										[텐텐클래스] (동작) 귀여운 진저맨 디저트 캔들 만들기
									</p>
									<p class="card-text text-center">55,000원</p>
								</div>
							</div>
						</div>
					</div>
					<!-- pagenation -->
					<div class="row justify-content-center">
						<div class="pagination">
						  <a href="#">&laquo;</a>
						  <a href="#">1</a>
						  <a href="#">2</a>
						  <a href="#">3</a>
						  <a href="#">4</a>
						  <a href="#">5</a>
						  <a href="#">6</a>
						  <a href="#">&raquo;</a>
						</div>
					</div>
				</div>
				
			</div>
			
				
		</div>
		<!-- fontawesome kit -->
      	<script src="https://kit.fontawesome.com/97dbc99ea1.js" crossorigin="anonymous"></script>
		<!-- js -->
		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
		<script>
		$(function(){ 
			  $(document).on("click","#far",function addClass() {
			    if($(this).attr('data-prefix') === "far"){
			      $(this).removeClass("far fa-heart fa-2x");
				   $(this).addClass("fas fa-heart fa-2x");
			    }else{
			      $(this).removeClass("fas fa-heart fa-2x");
			      $(this).addClass("far fa-heart fa-2x");
			    }
			  });
			});
		</script>
	
	</body>
</html>
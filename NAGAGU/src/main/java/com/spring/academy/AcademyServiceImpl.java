package com.spring.academy;

import java.util.ArrayList;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.mapper.AcademyMapper;

@Service
public class AcademyServiceImpl implements AcademyService {

	@Autowired
	private SqlSession sqlSession; // Mybatis(ibatis) 라이브러리가 제공하는 클래스
	
	@Override
	public ArrayList<ClassVO> getCls() {
		ArrayList<ClassVO> classList = new ArrayList<ClassVO>();
		AcademyMapper classMapper = sqlSession.getMapper(AcademyMapper.class); // getMmebers()의 메소드명과 mapper.xml의 id는 동일해야 한다.
		classList = classMapper.getCls(); /* xml문서에 있는 id가  getMembers인걸 실행함. */
		System.out.println(classMapper.getCount()); 
		
		return classList;
	}

	@Override
	public void insertClass(ClassVO academy) {
		AcademyMapper classMapper = sqlSession.getMapper(AcademyMapper.class);
		int res = classMapper.insertClass(academy); // 삽입후 삽입한 결과 상태 반환하기 위해 반환값을 int로 정해줌
		System.out.println("res = " + res);
	}
	
	@Override
	public int getCount() {
		int x = 0;
		int result = 0;
		
		AcademyMapper classMapper = sqlSession.getMapper(AcademyMapper.class);
		result = classMapper.getCount();
		
		if(result != 0)
			x = result;
		
		return x;
	}

}

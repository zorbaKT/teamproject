<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
   content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- Bootstrap CSS -->
<link rel="stylesheet"
   href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
   integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
   crossorigin="anonymous">
<%-- <link rel="stylesheet" type="text/css"
   href="${pageContext.request.contextPath}/resources/css/Community/detail.css"> --%>
	<style>
	@font-face {
	   font-family: 'KOMACON';
	   src:
	      url('https://cdn.jsdelivr.net/gh/projectnoonnu/noonfonts_seven@1.2/KOMACON.woff')
	      format('woff');
	   font-weight: normal;
	   font-style: normal;
	}
	body {
	   font-family: '만화진흥원체', 'KOMACON', KOMACON !important;
	   font-size: 15px;
	}
	img {
	   width: 100%;
	   height: 100%;
	}
	a, a:link, a:hover {
	   text-decoration: none;
	   color: black;;
	}
	.title>div {
	   background-color: ;
	   color: rgba(0, 0, 0, 0.8);
	   font-size: 1rem !important;
	}
	.main>.title {
	   margin-left: 0 !important;
	   margin-right: 0 !important;
	   padding: 0px auto;
	}
	.img-wrap {
	   height: auto;
	}
	.main {
	   width: 75%;
	   height: auto;
	}
	.sidebar {
	   width: 25%;
	   height: 25vh;
	}
	.sidebar {
	   position: -webkit-sticky;
	   position: sticky;
	   top: 0;
	}
	/* 댓글 */
	.name {
		font-weight: bold;
	}
	.smallfont {
		font-size: 0.7em;
	}
	hr {
		background-color: #EF902E;
	}
	.rep_content {
		font-size: 1.0em;
	}
	.btn_write {
		width:37px;
		padding:3%;
	    border-radius: 4px;
	    border:1px solid orangered;
	    color:orangered;
	    font-size: 14px;
	}
	.comments_table {
	   font-size: 1rem;
	}
	@media ( max-width : 700px) {
	   .comments_table {
	      font-size: 0.7rem;
	   }
	}
	
	.container-mypage{
	   margin: 50px 0 100px 0 ;
	}
	.page-tap a{
		color: black;	
	}
	.page-tap a:hover{
		color: #ef900e !important;
		transform: scale(1.2);
		text-decoration: none;
	}
	.profile-img img{
	  	width: 30px;
	    height: auto;
	    margin-right: 10px;
	}
	/* follow button */
	.follow-btn{
		border: none;
		background: #ef900e;	
		font-size: 1rem;	 
		border-radius: 10px;
		transition:all 0.2s; 
		box-shadow: 0px 3px rgba(239,144,14,0.5); 	 
		color: white;
	}
	.follow-btn:active{		
			transform: translateY(3px);
	}
	 *:focus { 
	 	outline:none !important; 
	 }
	 /* follow button end*/
	</style>
<title>Mypage</title>
</head>
<body>
   <div class="container container-mypage">
      <div class="wrapper row justify-content-between">
         <div class="main pb-5">
            <div class="row justify-content-between title">
               <div class="col-10 page-tap">
                  <h6><a href="community.cm">COMMUNITY</a> > 상세보기 </h6>
               </div> 
            </div>
            <div class="row img-wrap ">
               <div class="col-10 pb-5">
                  <a href=""><img
                     src="https://images.unsplash.com/photo-1552584010-ca8bbbd5bd18?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=400&fit=max&ixid=eyJhcHBfaWQiOjYxOTE2fQ"
                     alt="" /></a>
               </div>
               <div class="col-10"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                  Enim impedit! Lorem ipsum dolor sit amet, consectetur adipisicing
                  elit. Ratione nihil omnis beatae modi optio necessitatibus quidem
                  quaerat labore enim aliquam!</p>
               </div>               
            </div>
         </div>
         <!-- main end -->
         <!-- sidebar start -->
         <div class="sidebar" id="sidebar">
            <div class="row justify-content-around pb-2">
               <div class="profile-img">
                  <img src="http://localhost:8000/DIYDIY/resources/images/Community/peko.png" width=1rem; alt="" class="src"  />hadong
               </div>
               <div class="follow-btn-wrap">                  
                  <button class="follow-btn">follow</button>
               </div>
            </div>
            <div class="d-inline-flex">
               <div class="">
                  <img
                     src="https://images.unsplash.com/photo-1552584010-ca8bbbd5bd18?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=400&fit=max&ixid=eyJhcHBfaWQiOjYxOTE2fQ"
                     alt="" class="img-responsive img-thumbnail" />
               </div>
               <div class="">
                  <img
                     src="https://images.unsplash.com/photo-1552584010-ca8bbbd5bd18?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=400&fit=max&ixid=eyJhcHBfaWQiOjYxOTE2fQ"
                     alt="" class="img-responsive img-thumbnail" />
               </div>
            </div>
            <div class="d-inline-flex">
               <div class="">
                  <img
                     src="https://images.unsplash.com/photo-1552584010-ca8bbbd5bd18?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=400&fit=max&ixid=eyJhcHBfaWQiOjYxOTE2fQ"
                     alt="" class="img-responsive img-thumbnail" />
               </div>
               <div class="">
                  <img
                     src="https://images.unsplash.com/photo-1552584010-ca8bbbd5bd18?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=400&fit=max&ixid=eyJhcHBfaWQiOjYxOTE2fQ"
                     alt="" class="img-responsive img-thumbnail" />
               </div>
            </div>
            <div class="d-flex justify-content-center pt-2">
               <div>
                  <i class="far fa-heart fa-2x"></i>15 <i
                     class="fas fa-share-alt-square fa-2x"></i>               
               </div>
            </div>
         </div>
         <!-- side bar end -->
      </div>
      <!-- wrapper end -->
      <!-- 댓글 테이블 시작 -->
		<br />
		<br />
		<hr />
		<h3 >Review</h3>
		<br /><br />
		<div class="reviews_table" style="color: #212529;">			 
			<div class="comment_add">
				<div class="row">
					<div class="col-1">
						<img src="${pageContext.request.contextPath}/resources/images/Community/peko.png" alt="" class="img-circle" style="width:60%; height:60%; margin:0 0 0 25px;">
					</div>
					<div class="col-11">
						<div class="row">
							<div class="col-10 name">김이름</div>
							<div class="col-2 smallfont px-0">2020-01-08</div>
						</div>
						<div class="row">
							<div class="col comm_content">예쁘게 만드셨네요!</div>
						</div>
						<div class="row">
							<div class="col">
								<a href="#" class="smallfont">답글달기</a> <a href="#"
									class="smallfont">신고하기</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<br/>
			<div class="comment-sum">
				<div class="row justify-content-between">
					<div class="col-1"></div>
					<div class="col-9">
						<textarea name="PICS_RE_CONTENT"
							placeholder="칭찬과 격려의 댓글은 작성자에게 큰 힘이 됩니다:)" style="width: 100%"
							rows="2"></textarea>
					</div>
					<div class="col-2 px-0">
						<a class="btn_write"
							onClick="location.href='./community_detail.cm'">&nbsp;등록</a>
					</div>
				</div>
			</div>						
		</div>
		<!-- 댓글 테이블 끝 -->
   </div>
   <!-- container end -->
   <!-- Optional JavaScript -->
   <!-- jQuery first, then Popper.js, then Bootstrap JS -->
   <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
      integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
      crossorigin="anonymous"></script>
   <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
      integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
      crossorigin="anonymous"></script>
   <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
      integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
      crossorigin="anonymous"></script>
      <!-- fontawesome kit -->
	<script src="https://kit.fontawesome.com/97dbc99ea1.js" crossorigin="anonymous"></script>
</body>
</html>
package com.spring.community;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
@Controller
public class CommunityController {
	

	@RequestMapping(value = "/community.cm")
	public String CommunityList() {
		
		return "Community/communityList";
	}
	
	@RequestMapping(value = "/community_detail.cm")
	public String CommunityDetail() {
		
		return "Community/detail";
	}
	
}

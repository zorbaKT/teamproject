package com.spring.mapper;

import java.util.ArrayList;

import com.spring.store.Product_qnaVO;

public interface ProductQnaMapper {
	ArrayList<Product_qnaVO> getqna(int QNA_PRODUCT);
}

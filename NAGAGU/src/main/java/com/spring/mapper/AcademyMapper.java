package com.spring.mapper;

import java.util.ArrayList;

import com.spring.academy.ClassVO;

public interface AcademyMapper {
	ArrayList<ClassVO> getCls();
	int insertClass(ClassVO academy);
	int getCount();
}

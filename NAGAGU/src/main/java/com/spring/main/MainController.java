package com.spring.main;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class MainController {
	
	@RequestMapping(value = "/index.ma")
	public String Index() {
		
		return "index";
	}

	@RequestMapping(value = "/login.ma")
	public String loginform() {
		
		return "login2";
	}
	
	@RequestMapping(value = "/signup.ma")
	public String signup() {
		
		return "signup";
	}
}
